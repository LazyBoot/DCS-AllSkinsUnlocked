livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_1BatGreece", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_1BatGreece_RoughMet", false};
	
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"AH-64D_bottom_1_1BatGreece", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"AH-64D_bottom_1_1BatGreece_RoughMet", false};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2_1BatGreece", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_1BatGreece_RoughMet", false};

--AH-64D_balka_1	
	{"AH-64D_balka_1",	DIFFUSE,			"AH-64D_balka_1_1BatGreece", false};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"AH-64D_balka_1_1BatGreece_RoughMet", false};

--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_1BatGreece", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_1BatGreece_RoughMet", false};
	
	-- AH-64D_chassis
	{"AH-64D_chassis",	DIFFUSE,			"ah-64d_chassis", false};
	{"AH-64D_chassis",	NORMAL_MAP,			"ah-64d_chassis_normal", false};
	{"AH-64D_chassis",	ROUGHNESS_METALLIC,	"ah-64d_chassis_roughmet", false};

	
	-- AH-64D_engine_2
	{"AH-64D_engine_1",	DIFFUSE,			"ah-64d_engine_1", false};
	
	
	-- AH-64D_tire_L
	{"AH-64D_tire_L",	DIFFUSE,			"ah-64d_tires", true};


-- AH-64D_tire_R
	{"AH-64D_tire_R",	DIFFUSE,			"ah-64d_tires", true};
	
	{"AH-64D_tires_D",	DIFFUSE,			"ah-64d_tires_D", true};


--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"empty", true};
	{"AH64D_fin_bort_number",	DIFFUSE,	"empty", true};	


-- PILOT-------------------------------------------------------------------------------------------

	{"AH-64D_pilot_body",	DIFFUSE	,						"AH-64D_pilot_body_1", false};
	{"AH-64D_pilot_body",	NORMAL_MAP,						"AH-64D_pilot_body_1_norm", false};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,				"AH-64D_pilot_body_1_RoughMet", false};
	{"AH-64D_pilot_details",	DIFFUSE,					"AH-64D_pilot_details", false};
	{"AH-64D_pilot_details",	NORMAL_MAP,					"AH-64D_pilot_details_Normal", true};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,			"AH-64D_pilot_details_RoughMet", true};
}



name = "Pegasus Display Team - ES1008, Hellenic Army Aviation"



custom_args = 
{
[553] = 1.0, --Remove Balacalva
[552] = 1.0, --Remove Balacalva
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
