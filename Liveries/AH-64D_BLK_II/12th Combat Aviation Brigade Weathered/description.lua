livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"ah-64d_front", false};
	{"AH-64D_front",	NORMAL_MAP,			"ah-64d_front_normal", true};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"ah-64d_front_roughmet", true};
	
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"ah-64d_bottom_1", false};
	{"AH-64D_bottom_1",	NORMAL_MAP,			"ah-64d_bottom_1_normal", true};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"ah-64d_bottom_1_roughmet", true};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"ah-64d_bottom_2", false};
	{"AH-64D_bottom_2",	NORMAL_MAP,			"ah-64d_bottom_2_normal", true};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"ah-64d_bottom_2_roughmet", true};

-- AH-64D_gun
	{"AH-64D_gun",		DIFFUSE,			"ah-64d_gun", false};
	{"AH-64D_gun",		NORMAL_MAP,			"ah-64d_gun_normal", true};
	{"AH-64D_gun",		ROUGHNESS_METALLIC,	"ah-64d_gun_roughmet", true};
	
-- AH-64D_gun_2	
	{"AH-64D_gun_2",	DIFFUSE,			"ah-64d_gun_2", true};
	{"AH-64D_gun_2",	NORMAL_MAP,			"ah-64d_gun_2_normal", true};
	{"AH-64D_gun_2",	ROUGHNESS_METALLIC,	"ah-64d_gun_2_roughmet", true};	

-- AH-64D_engine_1
	{"AH-64D_engine_1",	DIFFUSE,			"ah-64d_engine_1", false};
	{"AH-64D_engine_1",	NORMAL_MAP,			"ah-64d_engine_1_normal", true};
	{"AH-64D_engine_1",	ROUGHNESS_METALLIC,	"ah-64d_engine_1_roughmet", true};

-- AH-64D_engine_2
	{"AH-64D_engine_2",	DIFFUSE,			"ah-64d_engine_2", false};
	{"AH-64D_engine_2",	NORMAL_MAP,			"ah-64d_engine_2_normal", true};
	{"AH-64D_engine_2",	ROUGHNESS_METALLIC,	"ah-64d_engine_2_roughmet", true};

-- AH-64D_chassis
	{"AH-64D_chassis",	DIFFUSE,			"ah-64d_chassis", false};
	{"AH-64D_chassis",	NORMAL_MAP,			"ah-64d_chassis_normal", true};
	{"AH-64D_chassis",	ROUGHNESS_METALLIC,	"ah-64d_chassis_roughmet", true};

-- AH-64D_wings
	{"AH-64D_wings",	DIFFUSE,			"ah-64d_wings", false};
	{"AH-64D_wings",	NORMAL_MAP,			"ah-64d_wings_normal", true};
	{"AH-64D_wings",	ROUGHNESS_METALLIC,	"ah-64d_wings_roughmet", true};

-- AH-64D_podves_L
	{"AH-64D_podves_L",	DIFFUSE,			"ah-64d_podves_l", true};
	{"AH-64D_podves_L",	NORMAL_MAP,			"ah-64d_podves_l_normal", true};
	{"AH-64D_podves_L",	ROUGHNESS_METALLIC,	"ah-64d_podves_l_roughmet", true};

-- AH-64D_podves_R
	{"AH-64D_podves_R",	DIFFUSE,			"ah-64d_podves_r", true};
	{"AH-64D_podves_R",	NORMAL_MAP,			"ah-64d_podves_r_normal", true};
	{"AH-64D_podves_R",	ROUGHNESS_METALLIC,	"ah-64d_podves_r_roughmet", true};

--AH-64D_balka_1
	{"AH-64D_balka_1",	DIFFUSE,			"ah-64d_balka_1", false};
	{"AH-64D_balka_1",	NORMAL_MAP,			"ah-64d_balka_1_normal", true};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"ah-64d_balka_1_roughmet", true};
	
--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"ah-64d_balka_2", false};
	{"AH-64D_balka_2",	NORMAL_MAP,			"ah-64d_balka_2_normal", true};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"ah-64d_balka_2_roughmet", true};

--AH-64D_vtulka
	{"AH-64D_vtulka",	DIFFUSE,			"ah-64d_vtulka", false};
	{"AH-64D_vtulka",	NORMAL_MAP,			"ah-64d_vtulka_normal", true};
	{"AH-64D_vtulka",	ROUGHNESS_METALLIC,	"ah-64d_vtulka_roughmet", true};

--AH-64D_lopast
	{"AH-64D_lopast",	DIFFUSE,			"ah-64d_lopast", false};
	{"AH-64D_lopast",	NORMAL_MAP,			"ah-64d_lopast_normal", true};
	{"AH-64D_lopast",	ROUGHNESS_METALLIC,	"ah-64d_lopast_roughmet", true};

--M261
	{"M261",			DIFFUSE,			"M261_Diff", false};
	
--M299
	{"M299",			DIFFUSE,			"M299_diff", false};	
		
--AGM_114K	
	{"AGM_114K",		DIFFUSE,			"AGM_114L_diff", false};
	
--AGM_114L
	{"AGM_114L",		DIFFUSE,			"AGM_114L_diff", false};

--AH-64D_pilot_patch_airborne
	{"AH-64D_pilot_patch",	 DIFFUSE,				"AH-64D_pilot_patch_airborne", false};
	{"AH-64D_pilot_patch",	 NORMAL_MAP,			"AH-64D_pilot_patch_airborne_Normal", false};
	{"AH-64D_pilot_patch",	 ROUGHNESS_METALLIC,	"AH-64D_pilot_patch_airborne_RoughMet", false};
	
	{"AH-64D_pilot_body",	DIFFUSE	,				"AH-64D_pilot_body", true};
	{"AH-64D_pilot_body",	NORMAL_MAP,				"AH-64D_pilot_body_Normal", true};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,		"AH-64D_pilot_body_RoughMet", true};



	{"pilot_AH64",    DIFFUSE            ,    "ah-64d_pilot_body", false};
    {"pilot_AH64",    NORMAL_MAP            ,    "ah-64d_pilot_body_normal", false};
    {"pilot_AH64",    SPECULAR            ,    "ah-64d_pilot_body_roughmet", false};
    {"pilot_AH64_details",    DIFFUSE            ,    "ah-64d_pilot_details", false};
    {"pilot_AH64_details",    NORMAL_MAP            ,    "ah-64d_pilot_details_normal", false};
    {"pilot_AH64_details",    SPECULAR            ,    "ah-64d_pilot_details_roughmet", false};
    {"pilot_AH64_patch",    DIFFUSE            ,    "ah-64d_pilot_patch_airborne", false};
    {"pilot_AH64_patch",    NORMAL_MAP            ,    "ah-64d_pilot_patch_airborne_normal", false};
    {"pilot_AH64_patch",    SPECULAR            ,    "ah-64d_pilot_patch_airborne_roughmet", false};
    --{"pilot_AH64_helmet_shell",    DIFFUSE            ,    "ah-64d_pilot_helmet_shell", false};
    --{"pilot_AH64_helmet_shell",    NORMAL_MAP            ,    "ah-64d_pilot_helmet_shell_normal", false};
    --{"pilot_AH64_helmet_shell",    SPECULAR            ,    "ah-64d_pilot_helmet_shell_roughmet", false};
    --{"pilot_AH64_helmet_details",    DIFFUSE            ,    "ah-64d_pilot_helmet_details", false};
    --{"pilot_AH64_helmet_details",    NORMAL_MAP            ,    "ah-64d_pilot_helmet_details_normal", false};
    --{"pilot_AH64_helmet_details",    SPECULAR            ,    "ah-64d_pilot_helmet_details_roughmet", false};
    --{"pilot_AH64_helmet_glass",    DIFFUSE            ,    "ah-64d_pilot_glass", false};
    --{"pilot_AH64_helmet_glass",    SPECULAR            ,    "ah-64d_pilot_glass_roughmet", false};
    {"pilot_AH64_patch",    DIFFUSE            ,    "ah-64d_pilot_patch_airborne", false};
    {"pilot_AH64_patch",    NORMAL_MAP            ,    "ah-64d_pilot_patch_airborne_normal", false};
    {"pilot_AH64_patch",    SPECULAR            ,    "ah-64d_pilot_patch_airborne_roughmet", false};





}             


name = "12th Combat Aviation Brigade Weathered"





