livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_1BatGreece", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_1BatGreece_RoughMet", false};
	
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"AH-64D_bottom_1_1BatGreece", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"AH-64D_bottom_1_1BatGreece_RoughMet", false};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2_1BatGreece", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_1BatGreece_RoughMet", false};

--AH-64D_balka_1	
	{"AH-64D_balka_1",	DIFFUSE,			"AH-64D_balka_1_1BatGreece", false};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"AH-64D_balka_1_1BatGreece_RoughMet", false};

--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_1BatGreece", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_1BatGreece_RoughMet", false};


--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"AH-64D_decal_0_1BatGreece", false};
	{"AH64D_fin_bort_number",	DIFFUSE,	"AH-64D_number_1BatGreece", false};	


-- PILOT-------------------------------------------------------------------------------------------

	{"AH-64D_pilot_body",	DIFFUSE	,						"AH-64D_pilot_body", true};
	{"AH-64D_pilot_body",	NORMAL_MAP,						"AH-64D_pilot_body_Normal", true};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,				"AH-64D_pilot_body_RoughMet", true};

	{"AH-64D_pilot_details",	DIFFUSE,					"AH-64D_pilot_details", true};
	{"AH-64D_pilot_details",	NORMAL_MAP,					"AH-64D_pilot_details_Normal", true};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,			"AH-64D_pilot_details_RoughMet", true};
}



name = "1st Attack Helicopter Battalion, Hellenic Army Aviation"



custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
