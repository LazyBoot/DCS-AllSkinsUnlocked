livery = {
-- AH-64D_bottom_2
	{"AH-64D_bottom_1",	DIFFUSE,			"ah-64d_bottom_1_avengers", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"ah-64d_bottom_1_avengers_roughmet", false};


-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"ah-64d_bottom_2_avengers", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"ah-64d_bottom_2_avengers_roughmet", false};

}







name = "A Company, Avengers, 1-227th ARB"



custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
