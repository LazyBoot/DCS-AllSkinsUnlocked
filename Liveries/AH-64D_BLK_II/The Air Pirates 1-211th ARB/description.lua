livery = {
--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_Pirates", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_Pirates_RoughMet", false};

-- AH-64D_engine_2
	{"AH-64D_engine_2",	DIFFUSE,			"AH-64D_engine_2_Pirates", false};
	{"AH-64D_engine_2",	ROUGHNESS_METALLIC,	"AH-64D_engine_2_Pirates_RoughMet", false};

}







name = "A Company, The Air Pirates, 1-211th ARB UTNG"



custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
