livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_BaseColor", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_Roughness", false};
	
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"AH-64D_bottom_1_BaseColor", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"AH-64D_bottom_1_Roughness", false};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2_BaseColor", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_Roughness", false};

--AH-64D_balka_1	
	{"AH-64D_balka_1",	DIFFUSE,			"AH-64D_balka_1_BaseColor", false};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"AH-64D_balka_1_Roughness", false};

--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_BaseColor", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_Roughness", false};

--AH-64D_chassis
    {"AH-64D_chassis",	DIFFUSE,			"AH-64D_chassis_BaseColor", false};	
	{"AH-64D_chassis",	ROUGHNESS_METALLIC,	"AH-64D_chassis_Roughness", false};	
		
--AH-64D_engine_1
	{"AH-64D_engine_1",	DIFFUSE,			 "AH-64D_engine_1_BaseColor", false};
	{"AH-64D_engine_1",	ROUGHNESS_METALLIC,	 "AH-64D_engine_1_Roughness", false};
	
--AH-64D_engine_2
    {"AH-64D_engine_2",	DIFFUSE,			 "AH-64D_engine_2_BaseColor", false};
	{"AH-64D_engine_2",	ROUGHNESS_METALLIC,	 "AH-64D_engine_2_Roughness", false};

--AH-64D_gun
	{"AH-64D_gun",	DIFFUSE,			"AH-64D_gun_BaseColor", false};
	{"AH-64D_gun",	ROUGHNESS_METALLIC,	"AH-64D_gun_Roughness", false};
	
--AH-64D_lopast
	{"AH-64D_lopast",	DIFFUSE,			"AH-64D_lopast_BaseColor", false};
	{"AH-64D_lopast",	ROUGHNESS_METALLIC,	"AH-64D_lopast_Roughness", false};
	
--AH-64D_vtulka
	{"AH-64D_vtulka",	DIFFUSE,			"AH-64D_vtulka_BaseColor", false};
	{"AH-64D_vtulka",	ROUGHNESS_METALLIC,	"AH-64D_vtulka_Roughness", false};
	
--AH-64D_wings
	{"AH-64D_wings",	DIFFUSE,			"AH-64D_wings_BaseColor", false};
	{"AH-64D_wings",	ROUGHNESS_METALLIC,	"AH-64D_wings_Roughness", false};
	
	
--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"empty", true};
	{"AH64D_fin_bort_number",	DIFFUSE,	"empty", true};	


-- PILOT-------------------------------------------------------------------------------------------

	{"AH-64D_pilot_body",	DIFFUSE	,						"AH-64D_pilot_body_BaseColor", false};
	{"AH-64D_pilot_body",	NORMAL_MAP,						"AH-64D_pilot_body_Normal", false};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,				"AH-64D_pilot_body_Roughness", false};

	{"AH-64D_pilot_details",	DIFFUSE,					"AH-64D_pilot_details_BaseColor", false};
	{"AH-64D_pilot_details",	NORMAL_MAP,					"AH-64D_pilot_details_Normal", false};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,			"AH-64D_pilot_details_Roughness", false};
}



name = "1st Combat Helicopter Unit, Japanese Ground SDF"



--By Whim Artist

