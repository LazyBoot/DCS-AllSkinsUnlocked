livery = {
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"AH-64D_bottom_1_SilverSpurs", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"AH-64D_bottom_1_SilverSpurs_RoughMet", false};


-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2_SilverSpurs", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_SilverSpurs_RoughMet", false};

}







name = "A Troop, Silver Spurs, 3-17 CAV"



custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
