livery = {
--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_GrimReapers", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_GrimReapers_RoughMet", false};

-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2_GrimReapers", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_GrimReapers_RoughMet", false};

-- AH-64D_engine_2
	{"AH-64D_engine_2",	DIFFUSE,			"AH-64D_engine_2_GrimReapers", false};
	{"AH-64D_engine_2",	ROUGHNESS_METALLIC,	"AH-64D_engine_2_GrimReapers_RoughMet", false};
	
-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_GrimReapers", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_GrimReapers_RoughMet", false};
	

}







name = "B Company, Grim Reapers, 4-2 ARB"



custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
