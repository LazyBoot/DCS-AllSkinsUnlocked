--Aqil Huseynov (Please do not steal or manipulate the files.)
livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_SA", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_SA_RoughMet", false};
	
-- AH-64D_engine_1
	{"AH-64D_engine_1",	DIFFUSE,			"AH-64D_engine_1_SA", false};
	{"AH-64D_engine_1",	ROUGHNESS_METALLIC,	"AH-64D_engine_1_SA_RoughMet", false};

-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2_SA", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_SA_RoughMet", false};

--AH-64D_balka_1	
	{"AH-64D_balka_1",	DIFFUSE,			"AH-64D_balka_1_SA", false};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"AH-64D_balka_1_SA_RoughMet", false};

--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_SA", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_SA_RoughMet", false};

-- AH-64D_bottom_2
	{"AH-64D_bottom_1",	DIFFUSE,			"ah-64d_bottom_1_SA", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"ah-64d_bottom_1_SA_roughmet", false};


-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"ah-64d_bottom_2_SA", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"ah-64d_bottom_2_SA_roughmet", false};


--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"AH-64D_decal_0_SA", false};

-- PILOT-------------------------------------------------------------------------------------------

	{"AH-64D_pilot_body",	DIFFUSE	,						"AH-64D_pilot_body", true};
	{"AH-64D_pilot_body",	NORMAL_MAP,						"AH-64D_pilot_body_Normal", true};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,				"AH-64D_pilot_body_RoughMet", true};

	{"AH-64D_pilot_details",	NORMAL_MAP,					"AH-64D_pilot_details_Normal", true};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,			"AH-64D_pilot_details_RoughMet", true};

        {"AH-64D_pilot_helmet_details",	DIFFUSE			,	"ah-64d_pilot_helmet_details", false};
	{"AH-64D_pilot_helmet_details",	NORMAL_MAP			,	"ah-64d_pilot_helmet_details_normal", false};
	{"AH-64D_pilot_helmet_details",	ROUGHNESS_METALLIC			,	"AH-64D_pilot_helmet_details_RoughMet", false};
      
        {"AH-64D_pilot_details",	DIFFUSE			,	"ah-64d_pilot_details", false};




}



name = "Saudi Arabian National Guard"


custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
