livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_662SqZJ171", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_662SqZJ171_RoughMet", false};

--AH-64D_balka_1	
	{"AH-64D_balka_1",	DIFFUSE,			"AH-64D_balka_1_662SqZJ171", false};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"AH-64D_balka_1_662SqZJ171_RoughMet", false};

--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2_662SqZJ171", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_662SqZJ171_RoughMet", false};


--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"AH-64D_decal_0_664Sq9Rg", false};
	{"AH64D_fin_bort_number",	DIFFUSE,	"AH-64D_number_664Sq9Rg", false};	

-- PILOT-------------------------------------------------------------------------------------------

	{"AH-64D_pilot_body",	DIFFUSE	,						"AH-64D_pilot_body_USP", false};
	{"AH-64D_pilot_body",	NORMAL_MAP,						"AH-64D_pilot_body_USP_Normal", false};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,				"AH-64D_pilot_body_USP_RoughMet", false};

	{"AH-64D_pilot_details",	DIFFUSE,					"AH-64D_pilot_details_UCP", false};
	{"AH-64D_pilot_details",	NORMAL_MAP,					"AH-64D_pilot_details_UCP_Normal", false};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,			"AH-64D_pilot_details_UCP_RoughMet", false};
}



name = "662 Squadron 3 Regiment AAC UK - ZJ171"
name_ru = "662 эскадрилья, 3 полк ZJ171 UK"



custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
