livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"ah-64d_front", false};
	{"AH-64D_front",	NORMAL_MAP,			"ah-64d_front_normal", true};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"ah-64d_front_roughmet", false};
	
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"ah-64d_bottom_1", false};
	{"AH-64D_bottom_1",	NORMAL_MAP,			"ah-64d_bottom_1_normal", true};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"ah-64d_bottom_1_roughmet", true};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"ah-64d_bottom_2", false};
	{"AH-64D_bottom_2",	NORMAL_MAP,			"ah-64d_bottom_2_normal", true};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"ah-64d_bottom_2_roughmet", true};

-- AH-64D_gun
	{"AH-64D_gun",	DIFFUSE,				"ah-64d_gun", false};
	{"AH-64D_gun",	NORMAL_MAP,				"ah-64d_gun_normal", true};
	{"AH-64D_gun",	ROUGHNESS_METALLIC,		"ah-64d_gun_roughmet", true};
	
-- AH-64D_gun_2	
	{"AH-64D_gun_2",	DIFFUSE,			"ah-64d_gun_2", true};
	{"AH-64D_gun_2",	NORMAL_MAP,			"ah-64d_gun_2_normal", true};
	{"AH-64D_gun_2",	ROUGHNESS_METALLIC,	"ah-64d_gun_2_roughmet", true};	

-- AH-64D_engine_1
	{"AH-64D_engine_1",	DIFFUSE,			"ah-64d_engine_1", false};
	{"AH-64D_engine_1",	NORMAL_MAP,			"ah-64d_engine_1_normal", true};
	{"AH-64D_engine_1",	ROUGHNESS_METALLIC,	"ah-64d_engine_1_roughmet", true};

-- AH-64D_engine_2
	{"AH-64D_engine_2",	DIFFUSE,			"ah-64d_engine_2", false};
	{"AH-64D_engine_2",	NORMAL_MAP,			"ah-64d_engine_2_normal", true};
	{"AH-64D_engine_2",	ROUGHNESS_METALLIC,	"ah-64d_engine_2_roughmet", true};

-- AH-64D_chassis
	{"AH-64D_chassis",	DIFFUSE,			"ah-64d_chassis", false};
	{"AH-64D_chassis",	NORMAL_MAP,			"ah-64d_chassis_normal", true};
	{"AH-64D_chassis",	ROUGHNESS_METALLIC,	"ah-64d_chassis_roughmet", true};

-- AH-64D_tire_L
	{"AH-64D_tire_L",	DIFFUSE,			"ah-64d_tires", true};
	{"AH-64D_tire_L",	NORMAL_MAP,			"ah-64d_tires_normal", true};
	{"AH-64D_tire_L",	ROUGHNESS_METALLIC,	"ah-64d_tires_roughmet", true};

-- AH-64D_tire_R
	{"AH-64D_tire_R",	DIFFUSE,			"ah-64d_tires", true};
	{"AH-64D_tire_R",	NORMAL_MAP,			"ah-64d_tires_normal", true};
	{"AH-64D_tire_R",	ROUGHNESS_METALLIC,	"ah-64d_tires_roughmet", true};

-- AH-64D_tire_B	
	{"AH-64D_tires_B",	DIFFUSE,			"ah-64d_tires", true};
	{"AH-64D_tires_B",	NORMAL_MAP,			"ah-64d_tires_normal", true};
	{"AH-64D_tires_B",	ROUGHNESS_METALLIC,	"ah-64d_tires_roughmet", true};

-- AH-64D_wings
	{"AH-64D_wings",	DIFFUSE,			"ah-64d_wings", false};
	{"AH-64D_wings",	NORMAL_MAP,			"ah-64d_wings_normal", true};
	{"AH-64D_wings",	ROUGHNESS_METALLIC,	"ah-64d_wings_roughmet", true};

-- AH-64D_podves_L
	{"AH-64D_podves_L",	DIFFUSE,			"ah-64d_podves_l", true};
	{"AH-64D_podves_L",	NORMAL_MAP,			"ah-64d_podves_l_normal", true};
	{"AH-64D_podves_L",	ROUGHNESS_METALLIC,	"ah-64d_podves_l_roughmet", true};

-- AH-64D_podves_R
	{"AH-64D_podves_R",	DIFFUSE,			"ah-64d_podves_r", true};
	{"AH-64D_podves_R",	NORMAL_MAP,			"ah-64d_podves_r_normal", true};
	{"AH-64D_podves_R",	ROUGHNESS_METALLIC,	"ah-64d_podves_r_roughmet", true};

--AH-64D_balka_1
	{"AH-64D_balka_1",	DIFFUSE,			"ah-64d_balka_1", false};
	{"AH-64D_balka_1",	NORMAL_MAP,			"ah-64d_balka_1_normal", true};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"ah-64d_balka_1_roughmet", false};
	
--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"ah-64d_balka_2", false};
	{"AH-64D_balka_2",	NORMAL_MAP,			"ah-64d_balka_2_normal", true};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"ah-64d_balka_2_roughmet", true};

--AH-64D_vtulka
	{"AH-64D_vtulka",	DIFFUSE,			"ah-64d_vtulka", true};
	{"AH-64D_vtulka",	NORMAL_MAP,			"ah-64d_vtulka_normal", true};
	{"AH-64D_vtulka",	ROUGHNESS_METALLIC,	"ah-64d_vtulka_roughmet", true};

--AH-64D_lopast
	{"AH-64D_lopast",	DIFFUSE,			"ah-64d_lopast", true};
	{"AH-64D_lopast",	NORMAL_MAP,			"ah-64d_lopast_normal", true};
	{"AH-64D_lopast",	ROUGHNESS_METALLIC,	"ah-64d_lopast_roughmet", true};

--AH-64D_glass	
	{"AH-64D_glass",	DIFFUSE			,	"ah-64d_glass", true};
	{"AH-64D_glass",	ROUGHNESS_METALLIC			,	"ah-64d_glass_roughmet", true};
	{"AH-64D_glass",	14,	"ah-64d_glass_dif", true};




-- PILOT-------------------------------------------------------------------------------------------
	{"AH-64D_pilot_body",	DIFFUSE,			"ah-64d_pilot_body", false};
	{"AH-64D_pilot_body",	NORMAL_MAP,			"ah-64d_pilot_body_normal", false};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,	"ah-64d_pilot_body_roughmet", false};

	{"AH-64D_pilot_details",	DIFFUSE,			"ah-64d_pilot_details", false};
	{"AH-64D_pilot_details",	NORMAL_MAP,			"ah-64d_pilot_details_normal", true};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,	"ah-64d_pilot_details_roughmet", true};

	{"AH-64D_pilot_helmet",	DIFFUSE,			"ah-64d_pilot_helmet", false};
	{"AH-64D_pilot_helmet",	NORMAL_MAP,			"ah-64d_pilot_helmet_normal", true};
	{"AH-64D_pilot_helmet",	ROUGHNESS_METALLIC,	"ah-64d_pilot_helmet_roughmet", true};

	{"AH-64D_pilot_helmet_glass",	DIFFUSE,			"ah-64d_pilot_glass", true};
	{"AH-64D_pilot_helmet_glass",	ROUGHNESS_METALLIC,	"ah-64d_pilot_glass_roughmet", true};

	{"pilot_AH64_body",	DIFFUSE			,	"ah-64d_pilot_body", false};	
	{"pilot_AH64_details",	DIFFUSE			,	"ah-64d_pilot_details", false};
	{"pilot_AH64_body",	NORMAL_MAP			,	"ah-64d_pilot_body_normal", false};
	{"pilot_AH64_body",	SPECULAR			,	"ah-64d_pilot_body_roughmet", false};
	
	{"Pilot_AH64_jacket",	DIFFUSE			,	"pilot_ah64_fp_jacket", false};
	{"Pilot_AH64_jacket",	NORMAL_MAP			,	"pilot_ah64_fp_jacket_normal", false};
	{"Pilot_AH64_jacket",	SPECULAR			,	"pilot_ah64_fp_jacket_roughmet", false};
	{"Pilot_AH64_pouches",	DIFFUSE			,	"pilot_ah64_fp_pouches", false};
	{"Pilot_AH64_pouches",	NORMAL_MAP			,	"pilot_ah64_fp_pouches_normal", true};
	{"Pilot_AH64_pouches",	SPECULAR			,	"pilot_ah64_fp_pouches_roughmet", true};
	{"Pilot_AH64_pants",	DIFFUSE			,	"pilot_ah64_fp_pants", false};
	{"Pilot_AH64_pants",	NORMAL_MAP			,	"pilot_ah64_fp_pants_normal", true};
	{"Pilot_AH64_pants",	SPECULAR			,	"pilot_ah64_fp_pants_roughmet", true};

--COCKPIT-------------------------------------------------------------------------------------------
	{"AH-64D_cockpit_1_external",	DIFFUSE,			"ah-64d_cockpit_1_external", true};
	{"AH-64D_cockpit_1_external",	NORMAL_MAP,			"ah-64d_cockpit_1_external_normal", true};
	{"AH-64D_cockpit_1_external",	ROUGHNESS_METALLIC,	"ah-64d_cockpit_1_external_roughmet", true};


	{"AH-64D_cockpit_2_external",	DIFFUSE,			"ah-64d_cockpit_2_external", true};
	{"AH-64D_cockpit_2_external",	NORMAL_MAP,			"ah-64d_cockpit_2_external_normal", true};
	{"AH-64D_cockpit_2_external",	ROUGHNESS_METALLIC,	"ah-64d_cockpit_2_external_roughmet", true};
	
	{"AH-64D_cockpit_3_external",	DIFFUSE,			"ah-64d_cockpit_3_external", true};
	{"AH-64D_cockpit_3_external",	NORMAL_MAP,			"ah-64d_cockpit_3_external_normal", true};
	{"AH-64D_cockpit_3_external",	ROUGHNESS_METALLIC,	"ah-64d_cockpit_3_external_roughmet", true};
	
	{"AH-64D_cockpit_4_external",	DIFFUSE,			"ah-64d_cockpit_4_external", true};
	{"AH-64D_cockpit_4_external",	NORMAL_MAP,			"ah-64d_cockpit_4_external_normal", true};
	{"AH-64D_cockpit_4_external",	ROUGHNESS_METALLIC,	"ah-64d_cockpit_4_external_roughmet", true};
	
	{"AH-64D_glass_internal",	DIFFUSE,	"ah-64d_cockpit_glass_internal_refl", true};
	{"AH-64D_glass_internal",	DECAL,	"ah-64d_cockpit_glass_internal", true};
	
	{"AH-64D_pilot_patch",	DIFFUSE	,						"empty", true};
	{"AH-64D_pilot_patch",	NORMAL_MAP,						"empty", true};
	{"AH-64D_pilot_patch",	ROUGHNESS_METALLIC,				"empty", true};	

--Ordanace-------------------------------------------------------------------------------------------
	{"Fuel_tank_230",	DIFFUSE			,	"fuel_tank_230_diff", false};
	{"Fuel_tank_230",	SPECULAR			,	"fuel_tank_230_diff_roughmet", false};
	{"Fuel_tank_230",	NORMAL_MAP			,	"fuel_tank_230_nm", false};
	
--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"AH-64D_decal_0", true};
	{"AH64D_fin_bort_number",	DIFFUSE,	"AH-64D_number", true};	


}







name = "IAF 113th Hornet Squadron"
name_ru = "IAF-Saraf 113th Hornet Squadron"

--By ZedTank

order     = 999

custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}
