livery = {

-- AH-64D_front
	{"AH-64D_front",	DIFFUSE,			"AH-64D_front_1", false};
	{"AH-64D_front",	ROUGHNESS_METALLIC,	"AH-64D_front_1_Roughmet", false};
	
-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"AH-64D_bottom_1", false};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"AH-64D_bottom_1_Roughmet", false};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"AH-64D_bottom_2", false};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"AH-64D_bottom_2_Roughmet", false};

--AH-64D_balka_1	
	{"AH-64D_balka_1",	DIFFUSE,			"AH-64D_balka_1", false};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"AH-64D_balka_1_Roughmet", false};

--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"AH-64D_balka_2", false};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"AH-64D_balka_2_Roughmet", false};
    {"AH-64D_engine_2",	DIFFUSE			,	"ah-64d_engine_2", false};
    {"AH-64D_engine_1",	DIFFUSE         ,   "AH-64D_engine_1", false};
	
	{"AH-64D_engine_2",	ROUGHNESS_METALLIC			,	"ah-64d_engine_2_Roughmet", false};
    {"AH-64D_engine_1",	ROUGHNESS_METALLIC,			"AH-64D_engine_1_Roughmet", false};
	
	{"AH-64D_wings",	DIFFUSE			,	"ah-64d_wings", false};
	{"AH-64D_wings",	ROUGHNESS_METALLIC			,	"ah-64d_wings_Roughmet", false};
--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"empty", true};
	{"AH64D_fin_bort_number",	DIFFUSE,	"empty", true};	
    {"AH-64D_gun",	DIFFUSE	,						"AH-64D_gun", false};

{"AH-64D_glass",	14			,	"ah-64d_glass_dif", false};
-- PILOT-------------------------------------------------------------------------------------------

	{"AH-64D_pilot_body",	DIFFUSE	,						"AH-64D_pilot_body", false};
	{"AH-64D_pilot_body",	NORMAL_MAP,						"AH-64D_pilot_body_Normal", false};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,				"AH-64D_pilot_body_RoughMet", true};

	{"AH-64D_pilot_details",	DIFFUSE,					"AH-64D_pilot_details", false};
	{"AH-64D_pilot_details",	NORMAL_MAP,					"AH-64D_pilot_details_Normal", true};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,			"AH-64D_pilot_details_RoughMet", true};
}



name = "Indian Air Force - Gray"




