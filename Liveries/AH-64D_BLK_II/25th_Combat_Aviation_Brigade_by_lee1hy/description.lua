--2nd Squadron, 6th Calvary Regiment, 25th Combat Aviation Brigade, 25th Infantry Division

livery = {
	{"AH-64D_engine_2",	DIFFUSE			,	"ah-64d_engine_2", false};
	--{"AH-64D_engine_2",	NORMAL_MAP			,	"ah-64d_engine_2_normal", false};
	--{"AH-64D_engine_2",	ROUGHNESS_METALLIC			,	"ah-64d_engine_2_roughmet", false};
	
	{"AH-64D_wings",	DIFFUSE			,	"ah-64d_wings", false};
	--{"AH-64D_wings",	NORMAL_MAP			,	"ah-64d_wings_normal", false};
	--{"AH-64D_wings",	ROUGHNESS_METALLIC			,	"ah-64d_wings_roughmet", false};
	
	--{"AH-64D_wings",	10,	"ah-64d_wings_d_normal", false};
	{"AH-64D_chassis",	DIFFUSE			,	"ah-64d_chassis", false};
	--{"AH-64D_chassis",	NORMAL_MAP			,	"ah-64d_chassis_normal", false};
	--{"AH-64D_chassis",	ROUGHNESS_METALLIC			,	"ah-64d_chassis_roughmet", false};
	
	--{"AH-64D_chassis",	10,	"ah-64d_chassis_d_normal", false};
	{"AH-64D_vtulka",	DIFFUSE			,	"ah-64d_vtulka", false};
	--{"AH-64D_vtulka",	NORMAL_MAP			,	"ah-64d_vtulka_normal", false};
	--{"AH-64D_vtulka",	ROUGHNESS_METALLIC			,	"ah-64d_vtulka_roughmet", false};
	
	--{"AH-64D_vtulka",	10,	"ah-64d_vtulka_d_normal", false};
	{"AH-64D_gun",	DIFFUSE			,	"ah-64d_gun", false};
	--{"AH-64D_gun",	NORMAL_MAP			,	"ah-64d_gun_normal", false};
	--{"AH-64D_gun",	ROUGHNESS_METALLIC			,	"ah-64d_gun_roughmet", false};
	
	--{"AH-64D_gun",	10,	"ah-64d_gun_d_normal", false};
	{"AH-64D_gun",	DIFFUSE			,	"ah-64d_gun", false};
	--{"AH-64D_gun",	NORMAL_MAP			,	"ah-64d_gun_normal", false};
	--{"AH-64D_gun",	ROUGHNESS_METALLIC			,	"ah-64d_gun_roughmet", false};
	
	--{"AH-64D_gun",	10,	"ah-64d_gun_d_normal", false};
	{"AH-64D_tire_L",	DIFFUSE			,	"ah-64d_tires", false};
	--{"AH-64D_tire_L",	NORMAL_MAP			,	"ah-64d_tires_normal", false};
	--{"AH-64D_tire_L",	ROUGHNESS_METALLIC			,	"ah-64d_tires_roughmet", false};

	
	{"AH-64D_chassis",	DIFFUSE			,	"ah-64d_chassis", false};
	--{"AH-64D_chassis",	NORMAL_MAP			,	"ah-64d_chassis_normal", false};
	--{"AH-64D_chassis",	ROUGHNESS_METALLIC			,	"ah-64d_chassis_roughmet", false};
	
	--{"AH-64D_chassis",	10,	"ah-64d_chassis_d_normal", false};
	
	
	{"AH-64D_pilot_body",	DIFFUSE			,	"ah-64d_pilot_body", false};
	--{"AH-64D_pilot_body",	NORMAL_MAP			,	"ah-64d_pilot_body_usp_normal", false};
	--{"AH-64D_pilot_body",	ROUGHNESS_METALLIC			,	"ah-64d_pilot_body_usp_roughmet", false};
	{"AH-64D_pilot_helmet_shell",	DIFFUSE			,	"ah-64d_pilot_helmet_shell", false};
	--{"AH-64D_pilot_helmet_shell",	NORMAL_MAP			,	"ah-64d_pilot_helmet_shell_normal", false};
	--{"AH-64D_pilot_helmet_shell",	ROUGHNESS_METALLIC			,	"ah-64d_pilot_helmet_shell_roughmet", false};
	{"AH-64D_pilot_helmet_details",	DIFFUSE			,	"ah-64d_pilot_helmet_details", false};
	--{"AH-64D_pilot_helmet_details",	NORMAL_MAP			,	"ah-64d_pilot_helmet_details_normal", false};
	--{"AH-64D_pilot_helmet_details",	ROUGHNESS_METALLIC			,	"ah-64d_pilot_helmet_details_roughmet", false};

	{"AH-64D_pilot_helmet_details",	DIFFUSE			,	"ah-64d_pilot_helmet_details", false};
	--{"AH-64D_pilot_helmet_details",	NORMAL_MAP			,	"ah-64d_pilot_helmet_details_normal", false};
	--{"AH-64D_pilot_helmet_details",	ROUGHNESS_METALLIC			,	"ah-64d_pilot_helmet_details_roughmet", false};
	{"AH-64D_pilot_details",	DIFFUSE			,	"AH-64D_pilot_details", false};
	--{"AH-64D_pilot_details",	NORMAL_MAP			,	"ah-64d_pilot_details_ucp_normal", false};
	--{"AH-64D_pilot_details",	ROUGHNESS_METALLIC			,	"ah-64d_pilot_details_ucp_roughmet", false};
	
	{"AH-64D_front",	DIFFUSE			,	"ah-64d_front", false};
	--{"AH-64D_front",	NORMAL_MAP			,	"ah-64d_front_normal", false};
	--{"AH-64D_front",	ROUGHNESS_METALLIC			,	"ah-64d_front_roughmet", false};
	
	--{"AH-64D_front",	10,	"ah-64d_front_d_normal", false};


	{"AH-64D_tire_R",	DIFFUSE			,	"ah-64d_tires", false};
	--{"AH-64D_tire_R",	NORMAL_MAP			,	"ah-64d_tires_normal", false};
	--{"AH-64D_tire_R",	ROUGHNESS_METALLIC			,	"ah-64d_tires_roughmet", false};
	{"AH-64D_gun_2",	DIFFUSE			,	"ah-64d_gun_2", false};
	--{"AH-64D_gun_2",	NORMAL_MAP			,	"ah-64d_gun_2_normal", false};
	--{"AH-64D_gun_2",	ROUGHNESS_METALLIC			,	"ah-64d_gun_2_roughmet", false};
	{"AH-64D_lopast_NS",	DIFFUSE			,	"ah-64d_lopast", false};
	--{"AH-64D_lopast_NS",	NORMAL_MAP			,	"ah-64d_lopast_normal", false};
	{"AH-64D_lopast",	DIFFUSE			,	"ah-64d_lopast", false};
	--{"AH-64D_lopast",	NORMAL_MAP			,	"ah-64d_lopast_normal", false};
	--{"AH-64D_lopast",	ROUGHNESS_METALLIC			,	"ah-64d_lopast_roughmet", false};
	
	--{"AH-64D_lopast",	10,	"ah-64d_lopast_d_normal", false};
	{"AH-64D_lopast",	DIFFUSE			,	"ah-64d_lopast", false};
	--{"AH-64D_lopast",	NORMAL_MAP			,	"ah-64d_lopast_normal", false};
	--{"AH-64D_lopast",	ROUGHNESS_METALLIC			,	"ah-64d_lopast_roughmet", false};
	
	--{"AH-64D_lopast",	10,	"ah-64d_lopast_d_normal", false};
	{"AH-64D_lopast_v2",	DIFFUSE			,	"ah-64d_lopast", false};
	--{"AH-64D_lopast_v2",	NORMAL_MAP			,	"ah-64d_lopast_normal", false};
	--{"AH-64D_lopast_v2",	ROUGHNESS_METALLIC			,	"ah-64d_lopast_roughmet", false};

	--{"AH-64D_lopast_flip",	DIFFUSE			,	"ah-64d_inside_d", false};
	--{"AH-64D_lopast_flip",	NORMAL_MAP			,	"ah-64d_inside_d_normal", false};
	
	--{"AH-64D_lopast_flip",	ROUGHNESS_METALLIC	,	"ah-64d_inside_d_roughmet", false};
	{"AH-64D_bottom_2",	DIFFUSE			,	"ah-64d_bottom_2", false};
	--{"AH-64D_bottom_2",	NORMAL_MAP			,	"ah-64d_bottom_2_normal", false};
	--{"AH-64D_bottom_2",	ROUGHNESS_METALLIC			,	"ah-64d_bottom_2_roughmet", false};
	
	--{"AH-64D_bottom_2",	10,	"ah-64d_bottom_2_d_normal", false};
	
	{"AH-64D_engine_1",	DIFFUSE			,	"ah-64d_engine_1", false};
	--{"AH-64D_engine_1",	NORMAL_MAP			,	"ah-64d_engine_1_normal", false};
	--{"AH-64D_engine_1",	ROUGHNESS_METALLIC			,	"ah-64d_engine_1_roughmet", false};
	
	--{"AH-64D_engine_1",	10,	"ah-64d_engine_1_d_normal", false};
	
	{"AH-64D_balka_2",	DIFFUSE			,	"ah-64d_balka_2", false};
	--{"AH-64D_balka_2",	NORMAL_MAP			,	"ah-64d_balka_2_normal", false};
	--{"AH-64D_balka_2",	ROUGHNESS_METALLIC			,	"ah-64d_balka_2_roughmet", false};
	
	--{"AH-64D_balka_2",	10,	"ah-64d_balka_2_d_normal", false};
	{"AH-64D_balka_1",	DIFFUSE			,	"ah-64d_balka_1", false};
	--{"AH-64D_balka_1",	NORMAL_MAP			,	"ah-64d_balka_1_normal", false};
	--{"AH-64D_balka_1",	ROUGHNESS_METALLIC			,	"ah-64d_balka_1_roughmet", false};
	
	--{"AH-64D_balka_1",	10,	"ah-64d_balka_1_d_normal", false};
	
	{"AH-64D_balka_2",	DIFFUSE			,	"ah-64d_balka_2", false};
	--{"AH-64D_balka_2",	NORMAL_MAP			,	"ah-64d_balka_2_normal", false};
	--{"AH-64D_balka_2",	ROUGHNESS_METALLIC			,	"ah-64d_balka_2_roughmet", false};
	
	--{"AH-64D_balka_2",	10,	"ah-64d_balka_2_d_normal", false};
	{"AH-64D_tire_B",	DIFFUSE			,	"ah-64d_tires", false};
	--{"AH-64D_tire_B",	NORMAL_MAP			,	"ah-64d_tires_normal", false};
	--{"AH-64D_tire_B",	ROUGHNESS_METALLIC			,	"ah-64d_tires_roughmet", false};
	
	{"AH-64D_podves_R",	DIFFUSE			,	"ah-64d_podves_r", false};
	--{"AH-64D_podves_R",	NORMAL_MAP			,	"ah-64d_podves_r_normal", false};
	--{"AH-64D_podves_R",	ROUGHNESS_METALLIC			,	"ah-64d_podves_r_roughmet", false};
	
	{"AH-64D_podves_L",	DIFFUSE			,	"ah-64d_podves_l", false};
	--{"AH-64D_podves_L",	NORMAL_MAP			,	"ah-64d_podves_l_normal", false};
	--{"AH-64D_podves_L",	ROUGHNESS_METALLIC			,	"ah-64d_podves_l_roughmet", false};
	{"AH-64D_bottom_1",	DIFFUSE			,	"ah-64d_bottom_1", false};
	--{"AH-64D_bottom_1",	NORMAL_MAP			,	"ah-64d_bottom_1_normal", false};
	--{"AH-64D_bottom_1",	ROUGHNESS_METALLIC			,	"ah-64d_bottom_1_roughmet", false};
	
	--{"AH-64D_bottom_1",	10,	"ah-64d_bottom_1_d_normal", false};
	
	
	{"AGM_114K",	DIFFUSE			,	"agm_114k_diff", false};
	--{"AGM_114K",	NORMAL_MAP			,	"agm_114k_nm", false};
	{"AGM_114K",	ROUGHNESS_METALLIC			,	"agm_114k_diff_roughmet", false};
}


name = "2-6 CAV, 25th Combat Aviation Brigade"


