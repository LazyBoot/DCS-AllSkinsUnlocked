livery = {

-- ch-47f_tex01
	{"ch-47f_tex01", 0, "ch-47f_tex01_turk",	false};
	{"ch-47f_tex01", 1, "ch-47f_tex01_NRM",	true};
	{"ch-47f_tex01", ROUGHNESS_METALLIC, "ch-47f_tex01_RoughMet", true};	
	
-- ch-47f_tex01_BN
	{"ch-47f_tex01_BN", 0, "ch-47f_tex01_turk",	false};
	{"ch-47f_tex01_BN", 1, "ch-47f_tex01_NRM",	true};
	{"ch-47f_tex01_BN", ROUGHNESS_METALLIC, "ch-47f_tex01_RoughMet", true};
	{"ch-47f_tex01_BN", 3 ,"ch-47f_BN", true};
	
-- ch-47f_tex01_DECAL
	{"ch-47f_tex01_DECAL", 0, "ch-47f_tex01_turk",	false};
	{"ch-47f_tex01_DECAL", 1, "ch-47f_tex01_NRM",	true};
	{"ch-47f_tex01_DECAL", ROUGHNESS_METALLIC, "ch-47f_tex01_RoughMet", true};	
	{"ch-47f_tex01_DECAL", DECAL, "ch-47f_decal_turk", false};
	
-- ch-47f_tex02
	{"ch-47f_tex02", 0, "ch-47f_tex02_turk",	false};
	{"ch-47f_tex02", 1, "ch-47f_tex02_NRM",	true};
	{"ch-47f_tex02", ROUGHNESS_METALLIC, "ch-47f_tex02_RoughMet", true};	
	
-- ch-47f_tex03
	{"ch-47f_tex03", 0, "ch-47f_tex03_turk",	false};
	{"ch-47f_tex03", 1, "ch-47f_tex03_NRM",	true};
	{"ch-47f_tex03", ROUGHNESS_METALLIC, "ch-47f_tex03_RoughMet", true};	
	
-- ch-47f_tex04
	{"ch-47f_tex04", 0, "ch-47f_tex04_turk",	false};
	{"ch-47f_tex04", 1, "ch-47f_tex04_NRM",	true};
	{"ch-47f_tex04", ROUGHNESS_METALLIC, "ch-47f_tex04_RoughMet", true};		
	
-- ch-47f_tex05
	{"ch-47f_tex05", 0, "ch-47f_tex05_turk",	false};
	{"ch-47f_tex05", 1, "ch-47f_tex05_NRM",	true};
	{"ch-47f_tex05", ROUGHNESS_METALLIC, "ch-47f_tex05_RoughMet", true};	
	
-- ch-47f_tex06
	{"ch-47f_tex06", 0, "ch-47f_tex06_turk",	false};
	{"ch-47f_tex06", 1, "ch-47f_tex06_NRM",	true};
	{"ch-47f_tex06", ROUGHNESS_METALLIC, "ch-47f_tex06_RoughMet", true};		
	
-- ch-47f_tex07
	{"ch-47f_tex07", 0, "ch-47f_tex07_turk",	false};
	{"ch-47f_tex07", 1, "ch-47f_tex07_NRM",	true};
	{"ch-47f_tex07", ROUGHNESS_METALLIC, "ch-47f_tex07_RoughMet", true};	

-- ch-47f_tex08
	{"ch-47f_tex08", 0, "ch-47f_tex08_turk",	false};
	{"ch-47f_tex08", 1, "ch-47f_tex08_NRM",	true};
	{"ch-47f_tex08", ROUGHNESS_METALLIC, "ch-47f_tex08_RoughMet", true};	

-- ch-47f_tex09
	{"ch-47f_tex09", 0, "ch-47f_tex09_turk",	false};
	{"ch-47f_tex09", 1, "ch-47f_tex09_NRM",	true};
	{"ch-47f_tex09", ROUGHNESS_METALLIC, "ch-47f_tex09_RoughMet", true};	
	
-- ch-47f_tex09_DECAL
	{"ch-47f_tex09_DECAL", 0, "ch-47f_tex09_turk",	false};
	{"ch-47f_tex09_DECAL", 1, "ch-47f_tex09_NRM",	true};
	{"ch-47f_tex09_DECAL", ROUGHNESS_METALLIC, "ch-47f_tex09_RoughMet", true};
	{"ch-47f_tex09_DECAL", DECAL, "empty", true};
	
-- ch-47f_tex10
	{"ch-47f_tex10", 0, "ch-47f_tex10",	true};
	{"ch-47f_tex10", 1, "ch-47f_tex10_NRM",	true};
	{"ch-47f_tex10", ROUGHNESS_METALLIC, "ch-47f_tex10_RoughMet", true};	
	
-- ch-47f_tex11
	{"ch-47f_tex11", 0, "ch-47f_tex11_turk",	false};
	{"ch-47f_tex11", 1, "ch-47f_tex11_NRM",	true};
	{"ch-47f_tex11", ROUGHNESS_METALLIC, "ch-47f_tex11_RoughMet", true};
	
-- ch-47f_tex11_BN
	{"ch-47f_tex11_BN", 0, "ch-47f_tex11_turk",	false};
	{"ch-47f_tex11_BN", 1, "ch-47f_tex11_NRM",	true};
	{"ch-47f_tex11_BN", ROUGHNESS_METALLIC, "ch-47f_tex11_RoughMet", true};
	{"ch-47f_tex11_BN", 3 ,"empty", true};
	
-- ch-47f_tex12
	{"ch-47f_tex12", 0, "ch-47f_tex12_turk",	false};
	{"ch-47f_tex12", 1, "ch-47f_tex12_NRM",	true};
	{"ch-47f_tex12", ROUGHNESS_METALLIC, "ch-47f_tex12_RoughMet", true};
	
-- ch-47f_tex13
	{"ch-47f_tex13", 0, "ch-47f_tex13_turk",	false};
	{"ch-47f_tex13", 1, "ch-47f_tex13_NRM",	true};
	{"ch-47f_tex13", ROUGHNESS_METALLIC, "ch-47f_tex13_RoughMet", true};
	
-- ch-47f_tex14
	{"ch-47f_tex14", 0, "ch-47f_tex14_turk",	false};
	{"ch-47f_tex14", 1, "ch-47f_tex14_NRM",	true};
	{"ch-47f_tex14", ROUGHNESS_METALLIC, "ch-47f_tex14_RoughMet", true};
	
-- ch-47f_tex15
	{"ch-47f_tex15", 0, "ch-47f_tex15_turk",	false};
	{"ch-47f_tex15", 1, "ch-47f_tex15_NRM",	true};
	{"ch-47f_tex15", ROUGHNESS_METALLIC, "ch-47f_tex15_RoughMet", true};
	
-- ch-47f_tex16
	{"ch-47f_tex16", 0, "ch-47f_tex16_turk",	false};
	{"ch-47f_tex16", 1, "ch-47f_tex16_NRM",	true};
	{"ch-47f_tex16", ROUGHNESS_METALLIC, "ch-47f_tex16_RoughMet", true};
	
-- ch-47f_tex16_BN
	{"ch-47f_tex16_BN", 0, "ch-47f_tex16_turk",	false};
	{"ch-47f_tex16_BN", 1, "ch-47f_tex16_NRM",	true};
	{"ch-47f_tex16_BN", ROUGHNESS_METALLIC, "ch-47f_tex16_RoughMet", true};
	{"ch-47f_tex16_BN", 3 ,"empty", true};	
	
-- ch-47f_tex16_DECAL
	{"ch-47f_tex16_DECAL", 0, "ch-47f_tex16_turk",	false};
	{"ch-47f_tex16_DECAL", 1, "ch-47f_tex16_NRM",	true};
	{"ch-47f_tex16_DECAL", ROUGHNESS_METALLIC, "ch-47f_tex16_RoughMet", true};
	{"ch-47f_tex16_DECAL", DECAL, "empty", true};	
	
-- ch-47f_tex17
	{"ch-47f_tex17", 0, "ch-47f_tex17_turk",	false};
	{"ch-47f_tex17", 1, "ch-47f_tex17_NRM",	true};
	{"ch-47f_tex17", ROUGHNESS_METALLIC, "ch-47f_tex17_RoughMet", true};
	
-- Pilot






}

name = "Turkish Land Forces"
name_ru = "Сухопутные войска Турции"



--order     = 1
