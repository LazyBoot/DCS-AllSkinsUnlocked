livery = {
    {"JF-17_main08", 0, "JF-17_main08_PAF13", false},
    {"JF-17_main08", ROUGHNESS_METALLIC ,"JF-17_main08_PAF13_roughmet.dds",false},
    {"JF-17_main09", 0, "JF-17_main09_PAF13", false},
    {"JF-17_main09", ROUGHNESS_METALLIC ,"JF-17_main09_PAF13_roughmet.dds",false},
    {"JF-17_main08_decol", 0, "JF-17_main08_PAF13", false},
    {"JF-17_main08_decol", ROUGHNESS_METALLIC ,"JF-17_main08_PAF13_roughmet.dds",false},
    {"JF-17_main08_decol", 3, "JF-17_decol_15.dds", false},
    {"JF-17_main08_BN52", 0, "JF-17_main08_PAF13", false},
    {"JF-17_main08_BN52", ROUGHNESS_METALLIC ,"JF-17_main08_PAF13_roughmet.dds",false},
    {"JF-17_main08_BN31", 0, "JF-17_main08_PAF13", false},
    {"JF-17_main08_BN31", ROUGHNESS_METALLIC ,"JF-17_main08_PAF13_roughmet.dds",false},
    {"JF-17_main08_BN32", 0, "JF-17_main08_PAF13", false},
    {"JF-17_main08_BN32", ROUGHNESS_METALLIC ,"JF-17_main08_PAF13_roughmet.dds",false},
    {"JF-17_pilot_decol", 0, "JF-17_Pilot_PAF13_decol.tga", false},
}
name = "Pakistan Air Force No.16 Sqn Black Panthers (Reworked)"
name_cn = "巴基斯坦空军 第16黑豹中队 (新版涂装)"



order = 15
