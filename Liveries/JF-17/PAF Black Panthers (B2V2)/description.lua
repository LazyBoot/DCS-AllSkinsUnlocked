livery = {
    {"JF-17_main01", 0, "JF-17_main01_PAF12", false},
    {"JF-17_main01", ROUGHNESS_METALLIC ,"JF-17_main01_PAF12_roughmet.dds",false},
    {"JF-17_main01_BN52", 0, "JF-17_main01_PAF12", false},
    {"JF-17_main01_BN52", ROUGHNESS_METALLIC ,"JF-17_main01_PAF12_roughmet.dds",false},
    {"JF-17_main01_BN31", 0, "JF-17_main01_PAF12", false},
    {"JF-17_main01_BN31", ROUGHNESS_METALLIC ,"JF-17_main01_PAF12_roughmet.dds",false},
    {"JF-17_main01_BN32", 0, "JF-17_main01_PAF12", false},
    {"JF-17_main01_BN32", ROUGHNESS_METALLIC ,"JF-17_main01_PAF12_roughmet.dds",false},
    {"JF-17_main08", 0, "JF-17_main08_PAF12", false},
    {"JF-17_main08", ROUGHNESS_METALLIC ,"JF-17_main08_PAF12_roughmet.dds",false},
    {"JF-17_main08_decol", 0, "JF-17_main08_PAF12", false},
    {"JF-17_main08_decol", ROUGHNESS_METALLIC ,"JF-17_main08_PAF12_roughmet.dds",false},
    {"JF-17_main08_decol", 3, "JF-17_decol_17.dds", false},
    {"JF-17_main08_BN52", 0, "JF-17_main08_PAF12", false},
    {"JF-17_main08_BN52", ROUGHNESS_METALLIC ,"JF-17_main08_PAF12_roughmet.dds",false},
    {"JF-17_main08_BN31", 0, "JF-17_main08_PAF12", false},
    {"JF-17_main08_BN31", ROUGHNESS_METALLIC ,"JF-17_main08_PAF12_roughmet.dds",false},
    {"JF-17_main08_BN32", 0, "JF-17_main08_PAF12", false},
    {"JF-17_main08_BN32", ROUGHNESS_METALLIC ,"JF-17_main08_PAF12_roughmet.dds",false},
    {"JF-17_pilot_decol", 0, "JF-17_Pilot_PAF12_decol.tga", false},
}
name = "Pakistan Air Force No.16 Sqn Black Panthers (Block2 Camo2)"
name_cn = "巴基斯坦空军 第16黑豹中队 (Block2涂装2)"



order = 14
