livery = {
    {"JF-17_main08", 0, "JF-17_main08_PAF10", false},
    {"JF-17_main08", ROUGHNESS_METALLIC ,"JF-17_main08_PAF10_roughmet.dds",false},
    {"JF-17_main08_decol", 0, "JF-17_main08_PAF10", false},
    {"JF-17_main08_decol", ROUGHNESS_METALLIC ,"JF-17_main08_PAF10_roughmet.dds",false},
    {"JF-17_main08_BN52", 0, "JF-17_main08_PAF10", false},
    {"JF-17_main08_BN52", ROUGHNESS_METALLIC ,"JF-17_main08_PAF10_roughmet.dds",false},
    {"JF-17_main08_BN31", 0, "JF-17_main08_PAF10", false},
    {"JF-17_main08_BN31", ROUGHNESS_METALLIC ,"JF-17_main08_PAF10_roughmet.dds",false},
    {"JF-17_main08_BN32", 0, "JF-17_main08_PAF10", false},
    {"JF-17_main08_BN32", ROUGHNESS_METALLIC ,"JF-17_main08_PAF10_roughmet.dds",false},
    {"JF-17_pilot_decol", 0, "JF-17_Pilot_PAF10_decol.tga", false},
}
name = "Pakistan Air Force CCS Sqn Fierce Dragons"
name_cn = "巴基斯坦空军 战斗指挥官学校 枭龙中队"



order = 7
