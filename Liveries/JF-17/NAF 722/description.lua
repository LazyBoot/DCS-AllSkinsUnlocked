livery = {
    {"JF-17_main01", 0, "JF-17_main01_NAF01", false},
    {"JF-17_main01", ROUGHNESS_METALLIC ,"JF-17_main01_NAF01_roughmet.dds",false},
    {"JF-17_main01_BN52", 0, "JF-17_main01_NAF01", false},
    {"JF-17_main01_BN52", ROUGHNESS_METALLIC ,"JF-17_main01_NAF01_roughmet.dds",false},
    {"JF-17_main01_BN31", 0, "JF-17_main01_NAF01", false},
    {"JF-17_main01_BN31", ROUGHNESS_METALLIC ,"JF-17_main01_NAF01_roughmet.dds",false},
    {"JF-17_main01_BN32", 0, "JF-17_main01_NAF01", false},
    {"JF-17_main01_BN32", ROUGHNESS_METALLIC ,"JF-17_main01_NAF01_roughmet.dds",false},
    {"JF-17_main02", 0, "JF-17_main02_NAF01", false},
    {"JF-17_main02", ROUGHNESS_METALLIC ,"JF-17_main02_NAF01_roughmet.dds",false},
    {"JF-17_main03", 0, "JF-17_main03_NAF01", false},
    {"JF-17_main03", ROUGHNESS_METALLIC ,"JF-17_main03_NAF01_roughmet.dds",false},
    {"JF-17_main06", 0, "JF-17_main06_NAF01", false},
    {"JF-17_main06", ROUGHNESS_METALLIC ,"JF-17_main06_NAF01_roughmet.dds",false},
    {"JF-17_main07", 0, "JF-17_main07_NAF01", false},
    {"JF-17_main07", ROUGHNESS_METALLIC ,"JF-17_main07_NAF01_roughmet.dds",false},
    {"JF-17_main08", 0, "JF-17_main08_NAF01", false},
    {"JF-17_main08", ROUGHNESS_METALLIC ,"JF-17_main08_NAF01_roughmet.dds",false},
    {"JF-17_main08_decol", 0, "JF-17_main08_NAF01", false},
    {"JF-17_main08_decol", ROUGHNESS_METALLIC ,"JF-17_main08_NAF01_roughmet.dds",false},
    {"JF-17_main08_BN52", 0, "JF-17_main08_NAF01", false},
    {"JF-17_main08_BN52", ROUGHNESS_METALLIC ,"JF-17_main08_NAF01_roughmet.dds",false},
    {"JF-17_main08_BN31", 0, "JF-17_main08_NAF01", false},
    {"JF-17_main08_BN31", ROUGHNESS_METALLIC ,"JF-17_main08_NAF01_roughmet.dds",false},
    {"JF-17_main08_BN32", 0, "JF-17_main08_NAF01", false},
    {"JF-17_main08_BN32", ROUGHNESS_METALLIC ,"JF-17_main08_NAF01_roughmet.dds",false},
	{"JF-17_main01_BN52", 3, "empty", true},
    {"JF-17_main01_BN31", 3, "empty", true},
    {"JF-17_main01_BN32", 3, "empty", true},
    {"JF-17_main08_BN52", 3, "empty", true},
    {"JF-17_main08_BN31", 3, "empty", true},
    {"JF-17_main08_BN32", 3, "empty", true},
    {"JF-17_main08_decol", 3, "empty", true},
    {"JF-17_pilot_decol", 0, "empty", true},
}
name = "Nigerian Air Force 722"
name_cn = "尼日利亚空军 722架"


order = 78

