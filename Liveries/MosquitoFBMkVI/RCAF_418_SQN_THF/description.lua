livery = {
	{"mosquito_ext_01", 0 ,"01_D_418",false};	
	{"mosquito_ext_01", ROUGHNESS_METALLIC ,"01_D_418_RoughMet",false};	
	
	{"mosquito_ext_02", 0 ,"02_D_418",false};	
	{"mosquito_ext_02", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};	
	
	{"mosquito_ext_03", 0 ,"03_D_418",false};	
	{"mosquito_ext_03", ROUGHNESS_METALLIC ,"03_D_418_RoughMet",false};	
	
	{"mosquito_ext_04", 0 ,"04_D_418",false};	
	{"mosquito_ext_04", ROUGHNESS_METALLIC ,"04_D_418_RoughMet",false};	
	
	{"mosquito_ext_05", 0 ,"05_D_418",false};	
	{"mosquito_ext_05", ROUGHNESS_METALLIC ,"05_D_418_RoughMet",false};	
	
	{"mosquito_ext_06", 0 ,"06_D_418",false};	
	{"mosquito_ext_06", ROUGHNESS_METALLIC ,"06_D_418_RoughMet",false};	
	
	{"mosquito_ext_07", 0 ,"07_D_418",false};	
	{"mosquito_ext_07", ROUGHNESS_METALLIC ,"07_D_418_RoughMet",false};	
	
	{"mosquito_ext_08", 0 ,"08_D_418",false};	
	{"mosquito_ext_08", ROUGHNESS_METALLIC ,"08_D_418_RoughMet",false};	
	
	{"mosquito_ext_09", 0 ,"09_D_418",false};	
	{"mosquito_ext_09", ROUGHNESS_METALLIC ,"09_D_418_RoughMet",false};	
	
    {"mosquito 500 LB Bomb Fairing MK.III",	0 ,"mosquito_BombFairing_D", false};
	{"mosquito 500 LB Bomb Fairing MK.III",	ROUGHNESS_METALLIC ,"Mosquito_BombFairing_D_RoughMet", false};
	{"mosquito_50_Gallon_Drop_Tanks",	0 ,"mosquito_50_gallon_drop_tanks_d", false};
	{"mosquito_50_Gallon_Drop_Tanks",	ROUGHNESS_METALLIC ,"mosquito_50_gallon_drop_tanks_d_roughmet", false};
	{"mosquito_100_Gallon_Drop_Tanks",	0 ,"mosquito_100_gallon_drop_tanks_d", false};
	{"mosquito_100_Gallon_Drop_Tanks",	ROUGHNESS_METALLIC ,"mosquito_100_gallon_drop_tanks_d_roughmet", false};
	{"mosquito_Bomb Bay", 0 ,"Mosquito_Bomb_Bay_D_418",false};	
	{"mosquitoFBVI_int_01L", 0 ,"MosquitoFBVI_int01_D_418.dds",false};	
	{"mosquitoFBVI_int_01R", 0 ,"MosquitoFBVI_int01_D_418.dds",false};	
	{"mosquitoFBVI_int_02", 0 ,"MosquitoFBVI_int02_D_418.dds",false};	
	{"mosquito_blr", 0 ,"Mosquito_blr_blk",false};	
	{"mosquito_blr", ROUGHNESS_METALLIC ,"Mosquito_blr_blk_RoughMet",false};
	
	

	{"mosquito_NMR_T0", 0 ,"02_D_418",false};	
	{"mosquito_NMR_T0", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};	
	{"mosquito_NMR_T0", DECAL ,"empty",true};
	
	{"mosquito_NMR_T00", 0 ,"02_D_418",false};	
	{"mosquito_NMR_T00", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};	
	{"mosquito_NMR_T00", DECAL ,"empty",true};
	
	{"mosquito_NMR_T000", 0 ,"02_D_418",false};	
	{"mosquito_NMR_T000", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};	
	{"mosquito_NMR_T000", DECAL ,"empty",true};
	
	
	{"mosquito_NMR_SN", 0 ,"02_D_418",false};	
	{"mosquito_NMR_SN", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};	
	{"mosquito_NMR_SN", DECAL ,"empty",true};

	
	{"mosquito_NMR_SNN", 0 ,"02_D_418",false};	
	{"mosquito_NMR_SNN", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};
	{"mosquito_NMR_SNN", DECAL ,"empty",true};

	
	{"mosquito_NMR_S0", 0 ,"02_D_418",false};	
	{"mosquito_NMR_S0", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};
	{"mosquito_NMR_S0", DECAL ,"empty",true};

	
	{"mosquito_NMR_S00", 0 ,"02_D_418",false};	
	{"mosquito_NMR_S00", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};
	{"mosquito_NMR_S00", DECAL ,"empty",true};

	
	{"mosquito_NMR_S000", 0 ,"02_D_418",false};	
	{"mosquito_NMR_S000", ROUGHNESS_METALLIC ,"02_D_418_RoughMet",false};
	{"mosquito_NMR_S000", DECAL ,"empty",true};

	custom_args = 
{

-- Bort Numbers MMM NNSSS
[445] = -1.0;   -- Blank
[443] = -1.0;
[444] = -1.0;
[481] = -1.0;
[482] = -1.0;
[442] = -1.0;
[31] = -1.0;
[32] = -1.0;


-- OTHER
[0] = 0.0;        -- 0.0 Tail Wheel Extension
[1] = 0.0;        -- 0.0 Tail Wheel Suspension
[2] = 0.0;        -- 0.0 Tail Wheel Direction
[3] = 0.0;        -- 0.0 Landing Gear RHS
[4] = 0.0;        -- 0.0 Landing Gear Suspension RHS 
[5] = 0.0;        -- 0.0 Landing Gear LHS
[6] = 0.0;        -- 0.0 Landing Gear Suspension LHS 
[9] = 0.0;        -- 0.0 Flaps RHS
[10] = 0.0;        -- 0.0 Flaps LHS
[11] = 0.0;        -- 0.0 Aileron RHS
[12] = 0.0;        -- 0.0 Aileron LHS
[15] = 0.0;        -- 0.0 Elevator
[17] = 0.0;        -- 0.0 Rudder
[23] = 0.0;        -- 0.0 Wheel Chocks
[26] = 0.0;        -- 0.0 Bomb Bay Doors
[38] = 0.0;        -- 0.0 Entry Door
[50] = 0.0;        -- 0.0 Pilots Visible
[190] = 0.0;    -- 0.0 Port Nav Light
[191] = 0.0;    -- 0.0 Starboard Nav Light
[192] = 0.0;    -- 0.0 Tail Nav Light
[193] = 0.0;    -- 0.0 Nose Nav Light
[196] = 0.0;    -- 0.0 Port Aileron Light
[197] = 0.0;    -- 0.0 Starboard Aileron Light
[200] = 0.0;    -- 0.0 Identification Light RED
[201] = 0.0;    -- 0.0 Identification Light GREEN
[202] = 0.0;    -- 0.0 Identification Light AMBER
[203] = 0.0;    -- 0.0 Both Aileron Identification Lights - Multiple Colours "Smelted Lamp"?
[311] = 0.0;    -- 0.0 Bomb Rack LHS
[312] = 0.0;    -- 0.0 Rocket Rack LHS
[317] = 0.0;    -- 0.0 Bomb Rack RHS
[318] = 0.0;    -- 0.0 Rocket Rack RHS
[348] = 0.0;    -- 0.0 Cockpit Window LHS
[349] = 0.0;    -- 0.0 Cockpit Window RHS
[426] = 0.0;    -- 0.0 Weapons 20mm Cannons Visible
[427] = 0.0;    -- 0.0 Weapons Machine Guns Visible
[428] = 0.0;    -- 0.0 Weapon Access Panels Open/Closed
[413] = 0.0;    -- 0.0 Propellor Pitch LHS
[414] = 0.0;    -- 0.0 Propellor Pitch RHS
[475] = 0.0;    -- 0.1 Propellor Rotating LHS
[476] = 0.0;    -- 0.1 Propellor Rotating RHS

}
	
}
name = "RCAF 418 SQN THF 'Cousin Jake' 1944"
name_ru = "RCAF 418 SQN THF 'Cousin Jake' 1944"

