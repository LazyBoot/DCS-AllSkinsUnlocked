livery = {
	{"mosquito_ext_01", 0 ,"01",false};	
	{"mosquito_ext_01", ROUGHNESS_METALLIC ,"01RoughMet",false};	
	
	{"mosquito_ext_02", 0 ,"02",false};	
	{"mosquito_ext_02",	NORMAL_MAP			,	"02_n", false};
	{"mosquito_ext_02", ROUGHNESS_METALLIC ,"02RoughMet",false};	
	
	{"mosquito_ext_03", 0 ,"03",false};	
	{"mosquito_ext_03", ROUGHNESS_METALLIC ,"03RoughMet",false};	
	
	{"mosquito_ext_04", 0 ,"04",false};	
	{"mosquito_ext_04", ROUGHNESS_METALLIC ,"04RoughMet",false};	
	
	{"mosquito_ext_05", 0 ,"05",false};	
	{"mosquito_ext_05",	NORMAL_MAP			,	"05_n", false};
	{"mosquito_ext_05", ROUGHNESS_METALLIC ,"05RoughMet",false};	
	
	{"mosquito_ext_06", 0 ,"06",false};	
	{"mosquito_ext_06", ROUGHNESS_METALLIC ,"06RoughMet",false};	
	
	{"mosquito_ext_07", 0 ,"07",false};	
	{"mosquito_ext_07", ROUGHNESS_METALLIC ,"07RoughMet",false};	
	
	{"mosquito_ext_08", 0 ,"08",false};	
	{"mosquito_ext_08",	NORMAL_MAP			,	"08_n", false};
	{"mosquito_ext_08", ROUGHNESS_METALLIC ,"08RoughMet",false};	
	
	{"mosquito_ext_09", 0 ,"09",false};	
	{"mosquito_ext_09", ROUGHNESS_METALLIC ,"09RoughMet",false};
	
	{"Mosquito_wheel_R",	0		,	"mosquito_wheel_d", false};
	{"Mosquito_wheel_R",	NORMAL_MAP			,	"mosquito_wheel_n", true};
	{"Mosquito_wheel_R",	SPECULAR			,	"mosquito_wheel_d_roughmet", true};		
	
	{"Mosquito_wheel_L",	0		,	"mosquito_wheel_d", false};
	{"Mosquito_wheel_L",	NORMAL_MAP			,	"mosquito_wheel_n", true};
	{"Mosquito_wheel_L",	SPECULAR			,	"mosquito_wheel_d_roughmet", true};	
	
	{"Mosquito_wheel_Bac",	0		,	"mosquito_wheel_bac_d", false};
	{"Mosquito_wheel_Bac",	NORMAL_MAP			,	"mosquito_wheel_bac_n", true};
	{"Mosquito_wheel_Bac",	SPECULAR			,	"mosquito_wheel_bac_d_roughmet", true};	
	
	{"MosquitoFBVI_int_01",	0		,	"mosquitofbvi_int01_d", false};
	{"MosquitoFBVI_int_01",	NORMAL_MAP			,	"mosquitofbvi_int01_n", true};
	{"MosquitoFBVI_int_01",	SPECULAR			,	"mosquitofbvi_int01_d_roughmet", true};		
	
	{"MosquitoFBVI_int_01R",	0		,	"mosquitofbvi_int01_d", false};
	{"MosquitoFBVI_int_01R",	NORMAL_MAP			,	"mosquitofbvi_int01_n", true};
	{"MosquitoFBVI_int_01R",	SPECULAR			,	"mosquitofbvi_int01_d_roughmet", true};	

	{"MosquitoFBVI_int_01L",	0		,	"mosquitofbvi_int01_d", false};
	{"MosquitoFBVI_int_01L",	NORMAL_MAP			,	"mosquitofbvi_int01_n", true};
	{"MosquitoFBVI_int_01L",	SPECULAR			,	"mosquitofbvi_int01_d_roughmet", true};	
	
	{"MosquitoFBVI_int_02",	0		,	"mosquitofbvi_int02_d", false};
	{"MosquitoFBVI_int_02",	NORMAL_MAP			,	"mosquitofbvi_int02_n", true};
	{"MosquitoFBVI_int_02",	SPECULAR			,	"mosquitofbvi_int02_d_roughmet", true};	
	
	{"mosquito_blr",	0		,	"mosquito_blr", false};
	{"mosquito_blr", ROUGHNESS_METALLIC ,"mosquito_blr_RoughMet",false};

	
	{"pilot_mosquito",	0 ,	"pilot_mosquito", false};
	{"pilot_mosquito",	ROUGHNESS_METALLIC ,	"pilot_mosquito_roughmet", true};
	
	{"mosquito_NMR_T0", 0 ,"02",false};	
	{"mosquito_NMR_T0", ROUGHNESS_METALLIC ,"02RoughMet",false};	
	{"mosquito_NMR_T0", DECAL ,"Mosquito_number_b",false};	
	
	{"mosquito_NMR_T00", 0 ,"02",false};	
	{"mosquito_NMR_T00", ROUGHNESS_METALLIC ,"02RoughMet",false};	
	{"mosquito_NMR_T00", DECAL ,"Mosquito_number_b",false};
	
	{"mosquito_NMR_T000", 0 ,"02",false};	
	{"mosquito_NMR_T000", ROUGHNESS_METALLIC ,"02RoughMet",false};	
	{"mosquito_NMR_T000", DECAL ,"Mosquito_number_b",false};
	
	
	{"mosquito_NMR_SN", 0 ,"02",false};	
	{"mosquito_NMR_SN", ROUGHNESS_METALLIC ,"02RoughMet",false};	
	
	{"mosquito_NMR_SNN", 0 ,"02",false};	
	{"mosquito_NMR_SNN", ROUGHNESS_METALLIC ,"02RoughMet",false};
	
	{"mosquito_NMR_S0", 0 ,"02",false};	
	{"mosquito_NMR_S0", ROUGHNESS_METALLIC ,"02RoughMet",false};
	
	{"mosquito_NMR_S00", 0 ,"02",false};	
	{"mosquito_NMR_S00", ROUGHNESS_METALLIC ,"02RoughMet",false};
	
	{"mosquito_NMR_S000", 0 ,"02",false};	
	{"mosquito_NMR_S000", ROUGHNESS_METALLIC ,"02RoughMet",false};
	
	{"Mosquito 500 LB Bomb Fairing MK.III",	0		,	"mosquito 500 lb bomb fairing", false};
	{"Mosquito 500 LB Bomb Fairing MK.III",	ROUGHNESS_METALLIC			,	"mosquito 500 lb bomb fairing_roughmet", false};
	
	{"Mosquito_blade",	0		,	"mosquito_blade_d", false};
	{"Mosquito_blade",	NORMAL_MAP			,	"mosquito_blade_n", true};
	{"Mosquito_blade",	SPECULAR			,	"mosquito_blade_d_roughmet", false};	
}
name = "No. 27 Squadron RAF Popeye Camo Letters on"
name_ru = "No. 27 Squadron RAF Popeye Camo Letters on"

