livery = {
	{"mosquito_ext_01", 0 ,"01_D",false};	
	{"mosquito_ext_01", ROUGHNESS_METALLIC ,"01_D_RoughMet",true};	
	
	{"mosquito_ext_02", 0 ,"02_D",false};	
	{"mosquito_ext_02", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};	
	
	{"mosquito_ext_03", 0 ,"03_D",false};	
	{"mosquito_ext_03", ROUGHNESS_METALLIC ,"03_D_RoughMet",true};	
	
	{"mosquito_ext_04", 0 ,"04_D",false};	
	{"mosquito_ext_04", ROUGHNESS_METALLIC ,"04_D_RoughMet",false};	
	
	{"mosquito_ext_05", 0 ,"05_D",false};	
	{"mosquito_ext_05", ROUGHNESS_METALLIC ,"05_D_RoughMet",true};	
	
	{"mosquito_ext_06", 0 ,"06_D",false};	
	{"mosquito_ext_06", ROUGHNESS_METALLIC ,"06_D_RoughMet",true};	
	
	{"mosquito_ext_07", 0 ,"07_D",false};	
	{"mosquito_ext_07", ROUGHNESS_METALLIC ,"07_D_RoughMet",false};	
	
	{"mosquito_ext_08", 0 ,"08_D",false};	
	{"mosquito_ext_08", ROUGHNESS_METALLIC ,"08_D_RoughMet",true};	
	
	{"mosquito_ext_09", 0 ,"09_D",false};	
	{"mosquito_ext_09", ROUGHNESS_METALLIC ,"09_D_RoughMet",true};		
	
	{"mosquito_blr", 0 ,"Mosquito_blr", false};	


	-------------------------------------------------------------------------------------------------------------
	
		
	{"mosquito_NMR_T0", 0 ,"02_D",false};	
	{"mosquito_NMR_T0", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_T0", DECAL ,"empty",true};	
	
	{"mosquito_NMR_T00", 0 ,"02_D",false};	
	{"mosquito_NMR_T00", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_T00", DECAL ,"empty",true};
	
	{"mosquito_NMR_T000", 0 ,"02_D",false};	
	{"mosquito_NMR_T000", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_T000", DECAL ,"empty",true};
	
	
	{"mosquito_NMR_SN", 0 ,"02_D",false};	
	{"mosquito_NMR_SN", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_SN", DECAL ,"empty",true};	
	
	{"mosquito_NMR_SNN", 0 ,"02_D",false};	
	{"mosquito_NMR_SNN", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_SNN", DECAL ,"empty",true};	
	
	{"mosquito_NMR_S0", 0 ,"02_D",false};	
	{"mosquito_NMR_S0", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_S0", DECAL ,"empty",true};	
	
	{"mosquito_NMR_S00", 0 ,"02_D",false};	
	{"mosquito_NMR_S00", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_S00", DECAL ,"empty",true};	
	
	{"mosquito_NMR_S000", 0 ,"02_D",false};	
	{"mosquito_NMR_S000", ROUGHNESS_METALLIC ,"02_D_RoughMet",false};
	{"mosquito_NMR_S000", DECAL ,"empty",true};	
	

	-------------------------------------------------------------------------------------------------------------
	
		
	{"Mosquito 500 LB Bomb Fairing MK.III",	0 ,"10_BombFairing_D", true};
	{"Mosquito 500 LB Bomb Fairing MK.III",	ROUGHNESS_METALLIC ,"Mosquito 500 LB Bomb Fairing_RoughMet",true};

	{"Mosquito_50_Gallon_Drop_Tanks",	0 ,"11_Tanks50G_D", true};
	{"Mosquito_50_Gallon_Drop_Tanks",	ROUGHNESS_METALLIC ,"Mosquito_50_Gallon_Drop_Tanks_RoughMet", true};

	{"Mosquito_100_Gallon_Drop_Tanks",	0 ,"12_Tanks100G_D", true};
	{"Mosquito_100_Gallon_Drop_Tanks",	ROUGHNESS_METALLIC ,"Mosquito_100_Gallon_Drop_Tanks_RoughMet", true};
}

name = "RAF, ML897/D, No.1409 Met Flight, Wyton, late 1943"
name_ru = "RAF, ML897/D, No.1409 Met Flight, Wyton, late 1943"


