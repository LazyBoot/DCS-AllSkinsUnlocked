livery = {
	{"mosquito_ext_01", 0 ,"01_USSR",false};	
	{"mosquito_ext_01", ROUGHNESS_METALLIC ,"01_USSR_RoughMet",false};	
	
	{"mosquito_ext_02", 0 ,"02_USSR",false};	
	{"mosquito_ext_02", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};	
	
	{"mosquito_ext_03", 0 ,"03_USSR",false};	
	{"mosquito_ext_03", ROUGHNESS_METALLIC ,"03_USSR_RoughMet",false};	
	
	{"mosquito_ext_04", 0 ,"04_USSR",false};	
	{"mosquito_ext_04", ROUGHNESS_METALLIC ,"04_USSR_RoughMet",false};	
	
	{"mosquito_ext_05", 0 ,"05_USSR",false};	
	{"mosquito_ext_05", ROUGHNESS_METALLIC ,"05_USSR_RoughMet",false};	
	
	{"mosquito_ext_06", 0 ,"06_USSR",false};	
	{"mosquito_ext_06", ROUGHNESS_METALLIC ,"06_USSR_RoughMet",false};	
	
	{"mosquito_ext_07", 0 ,"07_USSR",false};	
	{"mosquito_ext_07", ROUGHNESS_METALLIC ,"07_USSR_RoughMet",false};	
	
	{"mosquito_ext_08", 0 ,"08_USSR",false};	
	{"mosquito_ext_08", ROUGHNESS_METALLIC ,"08_USSR_RoughMet",false};	
	
	{"mosquito_ext_09", 0 ,"09_USSR",false};	
	{"mosquito_ext_09", ROUGHNESS_METALLIC ,"09_USSR_RoughMet",false};	
	

	{"Mosquito_Bomb Bay", 0 ,"Mosquito_Bomb_Bay_D_No235",false};	
	{"MosquitoFBVI_int_01L", 0 ,"MosquitoFBVI_int01_D_No235.dds",false};	
	{"MosquitoFBVI_int_01R", 0 ,"MosquitoFBVI_int01_D_No235.dds",false};	
	{"MosquitoFBVI_int_02", 0 ,"MosquitoFBVI_int02_D_No235.dds",false};	
	{"mosquito_blr", 0 ,"Mosquito_blr_red",false};	
	{"mosquito_blr", ROUGHNESS_METALLIC ,"Mosquito_blr_red_RoughMet",false};	
	
	
	{"mosquito_NMR_T0", 0 ,"02_USSR",false};	
	{"mosquito_NMR_T0", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};	
	{"mosquito_NMR_T0", DECAL ,"empty",true};
	
	{"mosquito_NMR_T00", 0 ,"02_USSR",false};	
	{"mosquito_NMR_T00", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};	
	{"mosquito_NMR_T00", DECAL ,"Mosquito_number_r",false};
	
	{"mosquito_NMR_T000", 0 ,"02_USSR",false};	
	{"mosquito_NMR_T000", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};	
	{"mosquito_NMR_T000", DECAL ,"Mosquito_number_r",false};
	
	
	{"mosquito_NMR_SN", 0 ,"02_USSR",false};	
	{"mosquito_NMR_SN", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};	
	{"mosquito_NMR_SN", DECAL ,"empty",true};
	
	{"mosquito_NMR_SNN", 0 ,"02_USSR",false};	
	{"mosquito_NMR_SNN", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};
	{"mosquito_NMR_SNN", DECAL ,"empty",true};
	
	{"mosquito_NMR_S0", 0 ,"02_USSR",false};	
	{"mosquito_NMR_S0", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};
	{"mosquito_NMR_S0", DECAL ,"empty",true};
	
	{"mosquito_NMR_S00", 0 ,"02_USSR",false};	
	{"mosquito_NMR_S00", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};
	{"mosquito_NMR_S00", DECAL ,"empty",true};
	
	{"mosquito_NMR_S000", 0 ,"02_USSR",false};	
	{"mosquito_NMR_S000", ROUGHNESS_METALLIC ,"02_USSR_RoughMet",false};
	{"mosquito_NMR_S000", DECAL ,"empty",true};

}
name = "USSR Air Force"
name_ru = "USSR Air Force"

