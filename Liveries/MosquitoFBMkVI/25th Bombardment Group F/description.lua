livery = {
	{"mosquito_ext_01", 0 ,"MosFBVI_USAAF_25BG_01_D",false};	
	{"mosquito_ext_01", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_01_D_RoughMet",false};	
	
	{"mosquito_ext_02", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_ext_02", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	
	{"mosquito_ext_03", 0 ,"MosFBVI_USAAF_25BG_03_D",false};	
	{"mosquito_ext_03", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_03_D_RoughMet",false};	
	
	{"mosquito_ext_04", 0 ,"MosFBVI_USAAF_25BG_04_D",false};	
	{"mosquito_ext_04", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_04_D_RoughMet",false};	
	
	{"mosquito_ext_05", 0 ,"MosFBVI_USAAF_25BG_05_D",false};	
	{"mosquito_ext_05", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_05_D_RoughMet",false};	
	
	{"mosquito_ext_06", 0 ,"MosFBVI_USAAF_25BG_06_D",false};	
	{"mosquito_ext_06", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_06_D_RoughMet",false};	
	
	{"mosquito_ext_07", 0 ,"MosFBVI_USAAF_25BG_07_D",false};	
	{"mosquito_ext_07", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_07_D_RoughMet",false};	
	
	{"mosquito_ext_08", 0 ,"MosFBVI_USAAF_25BG_08_D",false};	
	{"mosquito_ext_08", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_08_D_RoughMet",false};	
	
	{"mosquito_ext_09", 0 ,"MosFBVI_USAAF_25BG_09_D",false};	
	{"mosquito_ext_09", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_09_D_RoughMet",false};	


	{"mosquito_NMR_T0", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_T0", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_T0", DECAL ,"empty",true};
	
	{"mosquito_NMR_T00", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_T00", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_T00", DECAL ,"empty",true};
	
	{"mosquito_NMR_T000", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_T000", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_T000", DECAL ,"empty",true};
	
	
	{"mosquito_NMR_SN", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_SN", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_SN", DECAL ,"Mosquito_number_blk604",false};

	
	{"mosquito_NMR_SNN", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_SNN", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_SNN", DECAL ,"Mosquito_number_blk604",false};

	
	{"mosquito_NMR_S0", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_S0", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_S0", DECAL ,"Mosquito_number_blk604",false};

	
	{"mosquito_NMR_S00", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_S00", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_S00", DECAL ,"Mosquito_number_blk604",false};

	
	{"mosquito_NMR_S000", 0 ,"MosFBVI_USAAF_25BG_F_02_D",false};	
	{"mosquito_NMR_S000", ROUGHNESS_METALLIC ,"MosFBVI_USAAF_25BG_F_02_D_RoughMet",false};	
	{"mosquito_NMR_S000", DECAL ,"Mosquito_number_blk604",false};

	{"mosquito_blr", 0 ,"Mosquito_blr_USAAF_25BG",false};	
	{"mosquito_blr", ROUGHNESS_METALLIC ,"Mosquito_blr_USAAF_25BG_RoughMet",false};
	
	{"Mosquito 500 LB Bomb Fairing MK.III",	DIFFUSE,	"mosquito 500 lb bomb fairing_AzBlue609", false};

	{"pilot_mosquito",	DIFFUSE,	"pilot_mosquito_usaaf", false};
	
}
name = 'USAAF 25th Bombardment Group "F"'


