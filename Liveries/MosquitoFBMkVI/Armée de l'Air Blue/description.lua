livery = {
	
	{"mosquito_ext_01",	0			,	"01", false};
	{"mosquito_ext_01",	13			,	"01_d_roughmet", true};
	
	{"mosquito_ext_02",	0			,	"02", false};
	{"mosquito_ext_02",	13			,	"02_roughmet", false};	
	
	{"mosquito_NMR_T0",	0			,	"02", false};
	{"mosquito_NMR_T0",	13			,	"02_roughmet", false};
	{"mosquito_NMR_T0",	    3				,	"mosquito_number_w", false};
	{"mosquito_NMR_T00",	0			,	"02", false};
	{"mosquito_NMR_T00",	13			,	"02_roughmet", false};
	{"mosquito_NMR_T00",	3				,	"mosquito_number_w", false};	
	{"mosquito_NMR_T000",	0			,	"02", false};
	{"mosquito_NMR_T000",	13			,	"02_roughmet", false};
	{"mosquito_NMR_T000",	3				,	"mosquito_number_w", false};
	
	{"mosquito_NMR_S0",	0			,	"02", false};
	{"mosquito_NMR_S0",	13			,	"02_roughmet", false};
	{"mosquito_NMR_S0",	3				,	"mosquito_number_b", false};	
	{"mosquito_NMR_S00",	0			,	"02", false};
	{"mosquito_NMR_S00",	13			,	"02_roughmet", false};
	{"mosquito_NMR_S00",	3				,	"mosquito_number_b", false};	
	{"mosquito_NMR_S000",	0			,	"02", false};
	{"mosquito_NMR_S000",	13			,	"02_roughmet", false};
	{"mosquito_NMR_S000",	3				,	"mosquito_number_b", false};
	
	{"mosquito_NMR_SN",	0			,	"02", false};
	{"mosquito_NMR_SN",	13			,	"02_roughmet", false};
	{"mosquito_NMR_SN",	3				,	"mosquito_number_b", false};	
	{"mosquito_NMR_SNN",	0			,	"02", false};
	{"mosquito_NMR_SNN",	13			,	"02_roughmet", false};
	{"mosquito_NMR_SNN",	3				,	"mosquito_number_b", false};
	
	{"mosquito_ext_03",	0			,	"03", false};
	{"mosquito_ext_03",	13			,	"03_d_roughmet", true};
	
	{"mosquito_ext_04",	0			,	"04", false};
	{"mosquito_ext_04",	13			,	"04_d_roughmet", true};
	
	{"mosquito_ext_05",	0			,	"05", false};
	{"mosquito_ext_05",	13			,	"05_d_roughmet", true};
	
	{"mosquito_ext_06",	0			,	"06", false};
	{"mosquito_ext_06",	13			,	"06_d_roughmet", true};
	
	{"mosquito_ext_07",	0			,	"07", false};
	{"mosquito_ext_07",	13			,	"07_d_roughmet", true};
	
	{"mosquito_ext_08",	0			,	"08", false};
	{"mosquito_ext_08",	13			,	"08_d_roughmet", true};
	
	{"mosquito_ext_09",	0			,	"09", false};
	{"mosquito_ext_09",	13			,	"09_d_roughmet", true};	
	
		{"Mosquito_50_Gallon_Drop_Tanks", 0 ,"Mosquito_50_Gallon_Drop_Tanks",false};
	{"Mosquito_100_Gallon_Drop_Tanks", 0 ,"Mosquito_100_Gallon_Drop_Tanks",false};

    {"mosquito_blr", 0 ,"Mosquito_blr_yel",false};	
	{"mosquito_blr", ROUGHNESS_METALLIC ,"Mosquito_blr_yel_RoughMet",false};	

	{"pilot_mosquito", 0 ,"pilot_mosquito", false};
	
}
name = "Armée de L'air Blue Camo"
 
