-- livery made by MJDixon
livery = {
	{"mosquito_ext_01", 0 ,"01_613",false};	
	{"mosquito_ext_01", ROUGHNESS_METALLIC ,"01_D_RoughMet",true};	
	
	{"mosquito_ext_02", 0 ,"02_613",false};	
	{"mosquito_ext_02", ROUGHNESS_METALLIC ,"02_613_RoughMet",false};	
	
	{"mosquito_ext_03", 0 ,"03_613",false};	
	{"mosquito_ext_03", ROUGHNESS_METALLIC ,"03_D_RoughMet",true};	
	
	{"mosquito_ext_04", 0 ,"04_613",false};	
	{"mosquito_ext_04", ROUGHNESS_METALLIC ,"04_613_RoughMet",false};	
	
	{"mosquito_ext_05", 0 ,"05_613",false};	
	{"mosquito_ext_05", ROUGHNESS_METALLIC ,"05_613_RoughMet",false};	
	
	{"mosquito_ext_06", 0 ,"06_613",false};	
	{"mosquito_ext_06", ROUGHNESS_METALLIC ,"06_D_RoughMet",true};	
	
	{"mosquito_ext_07", 0 ,"07_613",false};	
	{"mosquito_ext_07", ROUGHNESS_METALLIC ,"07_613_RoughMet",false};	
	
	{"mosquito_ext_08", 0 ,"08_613",false};	
	{"mosquito_ext_08", ROUGHNESS_METALLIC ,"08_613_RoughMet",false};	
	
	{"mosquito_ext_09", 0 ,"09_613",false};	
	{"mosquito_ext_09", ROUGHNESS_METALLIC ,"09_D_RoughMet",true};	
	
	{"mosquito_blr", 0 ,"Mosquito_blr_613",false};		
	
	{"mosquito_NMR_T0", 0 ,"02_613",false};	
	{"mosquito_NMR_T0", ROUGHNESS_METALLIC ,"02_613_RoughMet",false};	
	{"mosquito_NMR_T0", DECAL ,"Mosquito_number_613",false};	
	
	{"mosquito_NMR_T00", 0 ,"02_613",false};	
	{"mosquito_NMR_T00", ROUGHNESS_METALLIC ,"02_613_RoughMet",false};	
	{"mosquito_NMR_T00", DECAL ,"Mosquito_number_613",false};
	
	{"mosquito_NMR_T000", 0 ,"02_613",false};	
	{"mosquito_NMR_T000", ROUGHNESS_METALLIC ,"02_613_RoughMet",false};	
	{"mosquito_NMR_T000", DECAL ,"Mosquito_number_613",false};	
	
	{"mosquito_NMR_S00",	DIFFUSE			,	"02_613", false};
	{"mosquito_NMR_S00",	NORMAL_MAP			,	"02_n", true};
	{"mosquito_NMR_S00",	SPECULAR			,	"02_613_roughmet", false};
	{"mosquito_NMR_S00",	DECAL				,	"mosquito_number_b", true};
	{"mosquito_NMR_S00",	DAMAGE				,	"mosquito_ext2_damage", true};
	
	{"mosquito_NMR_S0",	DIFFUSE			,	"02_613", false};
	{"mosquito_NMR_S0",	NORMAL_MAP			,	"02_n", true};
	{"mosquito_NMR_S0",	SPECULAR			,	"02_613_roughmet", false};
	{"mosquito_NMR_S0",	DECAL				,	"mosquito_number_b", true};
	{"mosquito_NMR_S0",	DAMAGE				,	"mosquito_ext2_damage", true};
	
	{"mosquito_NMR_SN",	DIFFUSE			,	"02_613", false};
	{"mosquito_NMR_SN",	NORMAL_MAP			,	"02_n", true};
	{"mosquito_NMR_SN",	SPECULAR			,	"02_613_roughmet", false};
	{"mosquito_NMR_SN",	DECAL				,	"mosquito_number_b", true};
	{"mosquito_NMR_SN",	DAMAGE				,	"mosquito_ext2_damage", true};
	
	{"mosquito_NMR_SNN",	DIFFUSE			,	"02_613", false};
	{"mosquito_NMR_SNN",	NORMAL_MAP			,	"02_n", true};
	{"mosquito_NMR_SNN",	SPECULAR			,	"02_613_roughmet", false};
	{"mosquito_NMR_SNN",	DECAL				,	"mosquito_number_b", true};
	{"mosquito_NMR_SNN",	DAMAGE				,	"mosquito_ext2_damage", true};
	
	{"mosquito_NMR_S000",	DIFFUSE			,	"02_613", false};
	{"mosquito_NMR_S000",	NORMAL_MAP			,	"02_n", true};
	{"mosquito_NMR_S000",	SPECULAR			,	"02_613_roughmet", false};
	{"mosquito_NMR_S000",	DECAL				,	"mosquito_number_b", true};
	{"mosquito_NMR_S000",	DAMAGE				,	"mosquito_ext2_damage", true};	
	
	{"Mosquito 500 LB Bomb Fairing MK.III",	DIFFUSE			,	"mosquito 500 lb bomb fairing_613", false};
	{"Mosquito 500 LB Bomb Fairing MK.III",	NORMAL_MAP			,	"mosquito 500 lb bomb fairing_nm", true};
	{"Mosquito 500 LB Bomb Fairing MK.III",	SPECULAR			,	"mosquito 500 lb bomb fairing_613_roughmet", false};	

}
name = "No. 613 Squadron RAF, June 1944"


