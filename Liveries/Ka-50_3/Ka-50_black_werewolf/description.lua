livery = {

-- Ka_50_3_Glass - temporary	
	{"Ka_50_3_Glass", 0,                  "ka_50_glass_d",	true};
	{"Ka_50_3_Glass", 1,                  "ka_50_glass_n",	true};
	{"Ka_50_3_Glass", ROUGHNESS_METALLIC, "ka_50_glass_d_RoughMet", true};	 
	



-- Ka_50_3_nos
	{"Ka_50_3_nos", 0,	                "Ka_50_black_h347_werewolf_nos_D", false};
	{"Ka_50_3_nos", 1,	                "ka_50_nos_n",	true};
	{"Ka_50_3_nos", ROUGHNESS_METALLIC, "ka_50_nos_d_roughmet", true};
	
-- Ka_50_3_puzo 
	{"Ka_50_3_puzo", 0,                  "Ka_50_black_h347_werewolf_puzo_D", false};
	{"Ka_50_3_puzo", 1,                  "ka_50_puzo_n",	true};	
	{"Ka_50_3_puzo", ROUGHNESS_METALLIC, "ka_50_puzo_d_roughmet", true};
		
-- Ka_50_3_hvost
	{"Ka_50_3_hvost", 0,                  "Ka_50_black_h347_werewolf_hvost_D", false};
	{"Ka_50_3_hvost", 1,                  "ka_50_hvost_n",	true};	
	{"Ka_50_3_hvost", ROUGHNESS_METALLIC, "ka_50_hvost_d_roughmet", true};	

-- Ka_50_3_kriliya
	{"Ka_50_3_kriliya", 0,                  "Ka_50_black_h347_werewolf_krilo_D", false};
	{"Ka_50_3_kriliya", 1,                  "ka_50_krilo_n",	true};	
	{"Ka_50_3_kriliya", ROUGHNESS_METALLIC, "ka_50_krilo_d_roughmet", true};
	
-- Ka_50_3_Shassi
	{"Ka_50_3_Shassi", 0,                  "Ka_50_black_h347_werewolf_shassi_D", false};
	{"Ka_50_3_Shassi", 1,                  "ka_50_shassi_n",	true};	
	{"Ka_50_3_Shassi", ROUGHNESS_METALLIC, "ka_50_shassi_d_roughmet", true};		
	
-- Ka_50_3_Pushka
	{"Ka_50_3_Pushka", 0,                  "ka_50_pushka_d", true};
	{"Ka_50_3_Pushka", 1,                  "ka_50_pushka_n",	true};	
	{"Ka_50_3_Pushka", ROUGHNESS_METALLIC, "ka_50_pushka_d_roughmet", true};
	
-- Ka_50_3_Dvig_Vint_Redukt
	{"Ka_50_3_Dvig_Vint_Redukt", 0,                  "Ka_50_black_h347_werewolf_Vint_D", false};
	{"Ka_50_3_Dvig_Vint_Redukt", 1,                  "Ka_50_rotor_n",	true};	
	{"Ka_50_3_Dvig_Vint_Redukt", ROUGHNESS_METALLIC, "Ka_50_rotor_d_roughmet", true}; 
	
-- Ka_50_3_namber	
	{"Ka_50_3_namber",	0,	                "Ka_50_black_h347_werewolf_puzo_D", false};
	{"Ka_50_3_namber",	1,	                "ka_50_puzo_n",		true};
	{"Ka_50_3_namber",	ROUGHNESS_METALLIC,	"ka_50_puzo_d_roughmet",	true};
	{"Ka_50_3_namber",	DECAL,	            "empty",	true};	

-- Ka_50_3_kabina
	{"Ka_50_3_kabina", 0,                  "ka_50_cockpt_d", true};
	{"Ka_50_3_kabina", 1,                  "ka_50_cockpt_n",	true};	
	{"Ka_50_3_kabina", ROUGHNESS_METALLIC, "ka_50_cockpt_d_roughmet", true};	
		
-- Ka_50_3_Setki
	{"Ka_50_3_Setki", 0,                  "ka_50_glass_d", true};
	{"Ka_50_3_Setki", 1,                  "ka_50_glass_n",	true};	
	{"Ka_50_3_Setki", ROUGHNESS_METALLIC, "ka_50_glass_d_roughmet", true};	
	
	
-- Ka_50_3_Karkas
	{"Ka_50_3_Karkas", 0,                  "ka_50_karkas_d", true};
	{"Ka_50_3_Karkas", 1,                  "ka_50_karkas_n",	true};	
	{"Ka_50_3_Karkas", ROUGHNESS_METALLIC, "ka_50_karkas_d_roughmet", true}; 

		







-- pilot_KA50_body
	{"pilot_KA50_body", 0,                  "pilot_KA50_body_blue", false};
	{"pilot_KA50_body", 1,                  "pilot_ka50_body_nm",	true};	
	{"pilot_KA50_body", ROUGHNESS_METALLIC, "pilot_ka50_body_roughmet", true};

-- helmet_ZSH7_details
	{"helmet_ZSH7_details", 0,                  "pilot_ka50_helmet_details", true};
	{"helmet_ZSH7_details", 1,                  "pilot_ka50_helmet_details_shell_nm",	true};	
	{"helmet_ZSH7_details", ROUGHNESS_METALLIC, "pilot_ka50_helmet_details_shell_roughmet", true};


-- helmet_ZSH7_shell
	{"helmet_ZSH7_shell", 0,                  "pilot_ka50_helmet_details_shell", true};
	{"helmet_ZSH7_shell", 1,                  "pilot_ka50_helmet_details_shell_nm",	true};	
	{"helmet_ZSH7_shell", ROUGHNESS_METALLIC, "pilot_ka50_helmet_details_shell_roughmet", true}; 

-- helmet_ZSH7_glass
	{"helmet_ZSH7_glass", 0,                  "pilot_ka50_helmet_glass", true};	
	{"helmet_ZSH7_glass", ROUGHNESS_METALLIC, "pilot_ka50_helmet_glass_roughmet", true}; 




}



name =       "Black #020 Farnborough 1992 Werewolf"
name_ru =    'Чёрный #020, Фарнборо 1992 "Оборотень"'


order     = 1
