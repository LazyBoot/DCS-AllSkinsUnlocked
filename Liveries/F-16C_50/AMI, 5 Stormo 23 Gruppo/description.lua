-- livery made by PorcoRosso86

livery = {

-- F16_bl50_Main_1
	{"F16_bl50_Main_1",	0,	                "f16_bl50_main_1_AMI",			false};
	{"F16_bl50_Main_1",	1,	                "f16_bl50_main_1_AMI_normal",	false};
	{"F16_bl50_Main_1",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_AMI_roughmet", false};
	






-- F16_bl50_Main_2
	{"F16_bl50_Main_2",	0,	                "f16_bl50_main_2_AMI",			false};
	{"F16_bl50_Main_2",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_Main_2",	ROUGHNESS_METALLIC,	"f16_bl50_main_2_AMI_roughmet",	false};
	

	




-- F16_bl50_Main_3
	{"F16_bl50_Main_3",	0,	                "f16_bl50_main_3_AMI",			false};
	{"F16_bl50_Main_3",	1,	                "f16_bl50_main_3_AMI_normal",	false};
	{"F16_bl50_Main_3",	ROUGHNESS_METALLIC,	"f16_bl50_main_3_AMI_roughmet",	false};
	
-- F16_bl50_Kil
	{"F16_bl50_Kil",	0,	                "f16_bl50_kil_AMI",				false};
	{"F16_bl50_Kil",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_Kil",	ROUGHNESS_METALLIC,	"f16_bl50_kil_AMI_roughmet",	false};









-- F16_bl50_wing_L
	{"F16_bl50_wing_L",	0,	                "f16_bl50_wing_l_AMI",			false};
	{"F16_bl50_wing_L",	1,	                "f16_bl50_wing_l_normal",	true};
	{"F16_bl50_wing_L",	ROUGHNESS_METALLIC,	"f16_bl50_wing_l_AMI_roughmet",	false};
	
-- F16_bl50_wing_R
	{"F16_bl50_wing_R",	0,	                "f16_bl50_wing_r_AMI",			false};
	{"F16_bl50_wing_R",	1,	                "f16_bl50_wing_r_normal",	true};
	{"F16_bl50_wing_R",	ROUGHNESS_METALLIC,	"f16_bl50_wing_r_AMI_roughmet",	false};
	
-- F16_bl50_Engine
	{"F16_bl50_Engine",	0,	                "f16_bl50_engine_AMI",			false};
	{"F16_bl50_Engine",	1,	                "f16_bl50_engine_normal",	true};
	{"F16_bl50_Engine",	ROUGHNESS_METALLIC,	"f16_bl50_engine_roughmet",	true};



















-- F16_bl50_Wing_Pylon_1
	{"F16_bl50_Wing_Pylon_1", 0,	                "f16_bl50_wing_pylon_1",			true};
	{"F16_bl50_Wing_Pylon_1", 1,	                "f16_bl50_wing_pylon_1_normal",		true};
	{"F16_bl50_Wing_Pylon_1", ROUGHNESS_METALLIC,	"f16_bl50_wing_pylon_1_roughmet",	true};


-- F16_bl50_Wing_Pylon_2
	{"F16_bl50_Wing_Pylon_2", 0,	                "f16_bl50_wing_pylon_2",		    true};
	{"F16_bl50_Wing_Pylon_2", 1,	                "f16_bl50_wing_pylon_2_normal",		true};
	{"F16_bl50_Wing_Pylon_2", ROUGHNESS_METALLIC,	"f16_bl50_wing_pylon_2_roughmet",	true};
	


-- F16_bl50_LAU_129
	{"LAU_129", 0,	                                "LAU_129_diff",		                true};
	{"LAU_129", 1,	                                "LAU_129_NM",		                true};
	{"LAU_129", ROUGHNESS_METALLIC,	                "LAU_129_diff_RoughMet",	        true}; 

-- F16_bl50_Fuel_Tank_300Gal
	{"Fuel_Tank_300Gal", 0,	                        "Fuel_Tank_300Gal_AMI",		            false};
	{"Fuel_Tank_300Gal", 1,	                        "Fuel_Tank_300Gal_Normal",		    true};
	{"Fuel_Tank_300Gal", ROUGHNESS_METALLIC,	    "Fuel_Tank_300Gal_AMI_RoughMet",	    false};
	
-- F16_bl50_Fuel_Tank_370Gal
	{"F_16_Tank_370", 0,	                        "Fuel_Tank_370Gal_diff_AMI",		        false};
	{"F_16_Tank_370", 1,	                        "Fuel_Tank_370Gal_NM",		            true};
	{"F_16_Tank_370", ROUGHNESS_METALLIC,	        "Fuel_Tank_370Gal_diff_AMI_RoughMet",	    false};
	


-- Pilot
		
	{"pilot_F16_helmet", 0,	                    "pilot_f16_helmet",				true};
	{"pilot_F16_helmet", 1,	                    "pilot_f16_helmet_nm",			true};
	{"pilot_F16_helmet", ROUGHNESS_METALLIC,	"pilot_f16_helmet_roughmet",	true};
	
	{"pilot_F16_patch",	 0,	                    "pilot_f16_patch_AMI",				false};
	{"pilot_F16_patch",	 1,	                    "pilot_f16_patch_AMI_nm",			false};
	{"pilot_F16_patch",	 ROUGHNESS_METALLIC,	"pilot_f16_patch_AMI_roughmet",		false};
	
	{"pilot_F16",	     0,	                    "pilot_f16",			        true};
	{"pilot_F16",	     1,	                    "pilot_f16_nm",			        true};
	{"pilot_F16",	     ROUGHNESS_METALLIC,	"pilot_f16_roughmet",	        true};




--BORT_NUMBER----------------------------------------------------------------------------------------



--F16_bl50_FIN_DECAL	
    {"F16_bl50_FIN_DECAL",	0,	                "f16_bl50_kil_AMI",				false};
	{"F16_bl50_FIN_DECAL",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_DECAL",	ROUGHNESS_METALLIC,	"f16_bl50_kil_AMI_roughmet",	false};
	{"F16_bl50_FIN_DECAL",	DECAL,	            "F16_MM_72_Decal_AMI",	false};

--F16_bl50_FIN_BORT_NUMBER
    {"F16_bl50_FIN_BORT_NUMBER",	0,	                "f16_bl50_kil_AMI",				false};
	{"F16_bl50_FIN_BORT_NUMBER",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER",	ROUGHNESS_METALLIC,	"f16_bl50_kil_AMI_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER",	DECAL,	                  "F16_bort_number_AMI",	false};






--F16_bl50_NOUSE_DECAL
    {"F16_bl50_MAT1_BORT_NUMBER_X100",	0,	                "f16_bl50_main_1_AMI",			false};
	{"F16_bl50_MAT1_BORT_NUMBER_X100",	1,	                "f16_bl50_main_1_AMI_normal",	false};
	{"F16_bl50_MAT1_BORT_NUMBER_X100",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_AMI_roughmet", false};
	{"F16_bl50_MAT1_BORT_NUMBER_X100",	DECAL,	            "F16_bort_number_INVERTED_AMI", false};

--F16_bl50_REFUEL_DECAL 
    {"F16_bl50_MAT1_BORT_NUMBER_DECAL",	0,	                "f16_bl50_main_1_AMI",			false};
	{"F16_bl50_MAT1_BORT_NUMBER_DECAL",	1,	                "f16_bl50_main_1_AMI_normal",	false};
	{"F16_bl50_MAT1_BORT_NUMBER_DECAL",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_AMI_roughmet", false};
	{"F16_bl50_MAT1_BORT_NUMBER_DECAL",	DECAL,	            "empty", true};

--F16_bl50_NOUSE AND REFUEL BORT_NUMBER
    {"F16_bl50_MAT1_BORT_NUMBER",	0,	                "f16_bl50_main_1_AMI",			false};
	{"F16_bl50_MAT1_BORT_NUMBER",	1,	                "f16_bl50_main_1_AMI_normal",	false};
	{"F16_bl50_MAT1_BORT_NUMBER",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_AMI_roughmet", false};
	{"F16_bl50_MAT1_BORT_NUMBER",	DECAL,	            "empty", true};





--F16_bl50_INTAKE_BORT_NUMBER	
    {"F16_bl50_INTAKE_BORT_NUMBER",	0,	                "f16_bl50_main_3_AMI",				false};
	{"F16_bl50_INTAKE_BORT_NUMBER",	1,	                "f16_bl50_main_3_AMI_normal",		false};
	{"F16_bl50_INTAKE_BORT_NUMBER",	ROUGHNESS_METALLIC,	"f16_bl50_main_3_AMI_roughmet",	false};
	{"F16_bl50_INTAKE_BORT_NUMBER",	DECAL,	            "empty",	true};
}







name = "Italian Air Force, 5° Stormo, 23 Gruppo"


order     = 999

custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 1.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 1.0, -- vis nouse board number 
}

name = "Italian Air Force, 5° Stormo, 23 Gruppo"

