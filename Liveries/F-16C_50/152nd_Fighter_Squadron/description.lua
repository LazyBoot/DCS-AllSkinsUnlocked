livery = {

-- F16_bl50_Main_1
	{"F16_bl50_Main_1",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_Main_1",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_1_normal"};
	{"F16_bl50_Main_1",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	






-- F16_bl50_Main_2
	{"F16_bl50_Main_2",	0,	                "f16_bl50_main_2",			false};
	{"F16_bl50_Main_2",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_2_normal"};
	{"F16_bl50_Main_2",	ROUGHNESS_METALLIC,	"f16_bl50_main_2_roughmet",	false};
	

	




-- F16_bl50_Main_3
	{"F16_bl50_Main_3",	0,	                "f16_bl50_main_3",			false};
	{"F16_bl50_Main_3",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_3_normal"};
	{"F16_bl50_Main_3",	ROUGHNESS_METALLIC,	"f16_bl50_main_3_roughmet",	false};
	
-- F16_bl50_Kil
	{"F16_bl50_Kil",	0,	                "f16_bl50_kil",				false};
	{"F16_bl50_Kil",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_kil_normal"};
	{"F16_bl50_Kil",	ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};










-- F16_bl50_wing_L
	{"F16_bl50_wing_L",	0,	                "f16_bl50_wing_l",			false};
	{"F16_bl50_wing_L",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_wing_l_normal"};
	{"F16_bl50_wing_L",	ROUGHNESS_METALLIC,	"f16_bl50_wing_l_roughmet",	false};
	
-- F16_bl50_wing_R
	{"F16_bl50_wing_R",	0,	                "f16_bl50_wing_r",			false};
	{"F16_bl50_wing_R",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_wing_r_normal"};
	{"F16_bl50_wing_R",	ROUGHNESS_METALLIC,	"f16_bl50_wing_r_roughmet",	false};
	
-- F16_bl50_Engine
	{"F16_bl50_Engine",	0,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_engine"};
	{"F16_bl50_Engine",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_engine_normal"};
	{"F16_bl50_Engine",	ROUGHNESS_METALLIC,	"$LIVERIES/36th_Fighter_Squadron/f16_bl50_engine_roughmet"};



    {"F16_bl50_Tyres",0,                    "$LIVERIES/36th_Fighter_Squadron/F16_bl50_Tyres"};
	{"F16_bl50_Tyres",1,                    "$LIVERIES/36th_Fighter_Squadron/F16_bl50_Tyres_Normal"};
	{"F16_bl50_Tyres",ROUGHNESS_METALLIC  , "$LIVERIES/36th_Fighter_Squadron/F16_bl50_Tyres_RoughMet"};

	{"f16c_cockpit_1",	DIFFUSE	,	        "$LIVERIES/36th_Fighter_Squadron/f16c_cockpit_1"};
	{"f16c_cockpit_1",	NORMAL_MAP,	        "$LIVERIES/36th_Fighter_Squadron/f16c_cockpit_1_nrm"};















-- F16_bl50_Wing_Pylon_1
	{"F16_bl50_Wing_Pylon_1", 0,	                "f16_bl50_wing_pylon_1",			true};
	{"F16_bl50_Wing_Pylon_1", 1,	                "f16_bl50_wing_pylon_1_normal",		true};
	{"F16_bl50_Wing_Pylon_1", ROUGHNESS_METALLIC,	"f16_bl50_wing_pylon_1_roughmet",	true};


-- F16_bl50_Wing_Pylon_2
	{"F16_bl50_Wing_Pylon_2", 0,	                "f16_bl50_wing_pylon_2",		    true};
	{"F16_bl50_Wing_Pylon_2", 1,	                "f16_bl50_wing_pylon_2_normal",		true};
	{"F16_bl50_Wing_Pylon_2", ROUGHNESS_METALLIC,	"f16_bl50_wing_pylon_2_roughmet",	true};
	


-- F16_bl50_LAU_129
	{"LAU_129", 0,	                                "LAU_129_diff",		                true};
	{"LAU_129", 1,	                                "LAU_129_NM",		                true};
	{"LAU_129", ROUGHNESS_METALLIC,	                "LAU_129_diff_RoughMet",	        true}; 

-- F16_bl50_Fuel_Tank_300Gal
	{"Fuel_Tank_300Gal", 0,	                        "$LIVERIES/36th_Fighter_Squadron/Fuel_Tank_300Gal"};
	{"Fuel_Tank_300Gal", 1,	                        "$LIVERIES/36th_Fighter_Squadron/Fuel_Tank_300Gal_Normal"};
	{"Fuel_Tank_300Gal", ROUGHNESS_METALLIC,	    "$LIVERIES/36th_Fighter_Squadron/Fuel_Tank_300Gal_RoughMet"};
	
-- F16_bl50_Fuel_Tank_370Gal
	{"F_16_Tank_370", 0,	                        "$LIVERIES/36th_Fighter_Squadron/Fuel_Tank_370Gal_diff"};
	{"F_16_Tank_370", 1,	                        "Fuel_Tank_370Gal_NM",		            true};
	{"F_16_Tank_370", ROUGHNESS_METALLIC,	        "$LIVERIES/36th_Fighter_Squadron/Fuel_Tank_370Gal_diff_RoughMet"};
	


-- Pilot
		
	{"pilot_F16_helmet", 0,	                    "pilot_f16_helmet",				true};
	{"pilot_F16_helmet", 1,	                    "pilot_f16_helmet_nm",			true};
	{"pilot_F16_helmet", ROUGHNESS_METALLIC,	"pilot_f16_helmet_roughmet",	true};
	
	{"pilot_F16_patch",	 0,	                    "pilot_f16_patch",				false};
	{"pilot_F16_patch",	 1,	                    "pilot_f16_patch_nm",			false};
	{"pilot_F16_patch",	 ROUGHNESS_METALLIC,	"pilot_f16_patch_roughmet",		true};
	
	{"pilot_F16",	     0,	                    "pilot_f16",			        false};
	{"pilot_F16",	     1,	                    "pilot_f16_nm",			        true};
	{"pilot_F16",	     ROUGHNESS_METALLIC,	"pilot_f16_roughmet",	        true};




--BORT_NUMBER----------------------------------------------------------------------------------------



--F16_bl50_FIN_DECAL	
    {"F16_bl50_FIN_DECAL",	0,	                "f16_bl50_kil",				false};
	{"F16_bl50_FIN_DECAL",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_kil_normal"};
	{"F16_bl50_FIN_DECAL",	ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};
	{"F16_bl50_FIN_DECAL",	DECAL,	            "F16_AF_91_Decal",	false};

--F16_bl50_FIN_BORT_NUMBER
    {"F16_bl50_FIN_BORT_NUMBER",	0,	                "f16_bl50_kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_kil_normal"};
	{"F16_bl50_FIN_BORT_NUMBER",	ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER",	DECAL,	                  "F16_bort_number",	false};






--F16_bl50_NOUSE_DECAL
    {"F16_bl50_MAT1_BORT_NUMBER_X100",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_MAT1_BORT_NUMBER_X100",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_1_normal"};
	{"F16_bl50_MAT1_BORT_NUMBER_X100",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_MAT1_BORT_NUMBER_X100",	DECAL,	            "F16_bort_number", false};

--F16_bl50_REFUEL_DECAL 
    {"F16_bl50_MAT1_BORT_NUMBER_DECAL",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_MAT1_BORT_NUMBER_DECAL",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_1_normal"};
	{"F16_bl50_MAT1_BORT_NUMBER_DECAL",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_MAT1_BORT_NUMBER_DECAL",	DECAL,	            "F16_AF_91_Decal", false};

--F16_bl50_NOUSE AND REFUEL BORT_NUMBER
    {"F16_bl50_MAT1_BORT_NUMBER",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_MAT1_BORT_NUMBER",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_1_normal"};
	{"F16_bl50_MAT1_BORT_NUMBER",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_MAT1_BORT_NUMBER",	DECAL,	            "F16_bort_number", false};





--F16_bl50_INTAKE_BORT_NUMBER	
    {"F16_bl50_INTAKE_BORT_NUMBER",	0,	                "f16_bl50_main_3",				false};
	{"F16_bl50_INTAKE_BORT_NUMBER",	1,	                "$LIVERIES/36th_Fighter_Squadron/f16_bl50_main_3_normal"};
	{"F16_bl50_INTAKE_BORT_NUMBER",	ROUGHNESS_METALLIC,	"f16_bl50_main_3_roughmet",	false};
	{"F16_bl50_INTAKE_BORT_NUMBER",	DECAL,	            "F16_bort_number",	false};
}







name = "152nd Fighter Squadron 'Los Vaqueros'"



order     = 999

custom_args = 
{
 
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}

