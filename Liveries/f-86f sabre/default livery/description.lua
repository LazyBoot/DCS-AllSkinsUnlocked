livery = {

	{"F86_MAIN", 0 ,"F-86F_blank_DIF",false};
	{"F86_MAIN", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_MAIN", DECAL ,"F-86F_DECAL",true};
	
	
	{"F86_BORT_NUMBER_TAIL_R_01", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_TAIL_R_01", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_TAIL_R_01", DECAL ,"f-86f_bort_number_tail",true};
	
	
	{"F86_BORT_NUMBER_TAIL_R_10", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_TAIL_R_10", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_TAIL_R_10", DECAL ,"f-86f_bort_number_tail",true};
	
	
	{"F86_BORT_NUMBER_TAIL_R_100", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_TAIL_R_100", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_TAIL_R_100", DECAL ,"f-86f_bort_number_tail",true};
	
	
	{"F86_BORT_NUMBER_TAIL_L_01", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_TAIL_L_01", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_TAIL_L_01", DECAL ,"f-86f_bort_number_tail",true};
	
	
	{"F86_BORT_NUMBER_TAIL_L_10", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_TAIL_L_10", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_TAIL_L_10", DECAL ,"f-86f_bort_number_tail",true};
	
	
	{"F86_BORT_NUMBER_TAIL_L_100", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_TAIL_L_100", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_TAIL_L_100", DECAL ,"f-86f_bort_number_tail",true};	
	
	
	
	
	
	
	
	
	
	{"F86_BORT_NUMBER_R_01", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_R_01", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_R_01", DECAL ,"f-86f_bort_number",true};
	
	
	{"F86_BORT_NUMBER_R_10", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_R_10", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_R_10", DECAL ,"f-86f_bort_number",true};
	
	
	{"F86_BORT_NUMBER_R_100", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_R_100", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_R_100", DECAL ,"f-86f_bort_number",true};	
	
	
	
	{"F86_BORT_NUMBER_L_01", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_L_01", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_L_01", DECAL ,"f-86f_bort_number",true};
	
	
	{"F86_BORT_NUMBER_L_10", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_L_10", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_L_10", DECAL ,"f-86f_bort_number",true};
	
	
	{"F86_BORT_NUMBER_L_100", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_L_100", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_L_100", DECAL ,"f-86f_bort_number",true};











	{"F86_BORT_NUMBER_NOSE_R_01", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_NOSE_R_01", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_NOSE_R_01", DECAL ,"empty",true};
	
	
	{"F86_BORT_NUMBER_NOSE_R_10", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_NOSE_R_10", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_NOSE_R_10", DECAL ,"empty",true};
	
	
	{"F86_BORT_NUMBER_NOSE_R_100", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_NOSE_R_100", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_NOSE_R_100", DECAL ,"empty",true};	
	

	
	
	{"F86_BORT_NUMBER_NOSE_L_01", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_NOSE_L_01", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_NOSE_L_01", DECAL ,"empty",true};
	
	
	{"F86_BORT_NUMBER_NOSE_L_10", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_NOSE_L_10", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_NOSE_L_10", DECAL ,"empty",true};
	
	
	{"F86_BORT_NUMBER_NOSE_L_100", 0 ,"F-86F_blank_DIF",false};
	{"F86_BORT_NUMBER_NOSE_L_100", ROUGHNESS_METALLIC ,"F-86F_blank_DIF_RoughMet",false};
	{"F86_BORT_NUMBER_NOSE_L_100", DECAL ,"empty",true};
		
}
name = "default livery"
name_ru = "Камуфляж по умолчанию"

order     = 999
