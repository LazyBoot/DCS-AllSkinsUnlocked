livery = {
	{"SpitfireIX", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"SpitfireIX", ROUGHNESS_METALLIC ,"SpitfireIX_DIF_RoughMet",true};	

	{"NUMBER_L_0", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_L_0", DECAL ,"empty",true};
	{"NUMBER_L_00", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_L_00", DECAL ,"empty",true};
	{"NUMBER_L_000", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_L_000", DECAL ,"empty",true};

	{"NUMBER_L_F3_0", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_L_F3_0", DECAL ,"empty",true};
	{"NUMBER_L_F3_00", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_L_F3_00", DECAL ,"empty",true};
	{"NUMBER_L_F3_000", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_L_F3_000", DECAL ,"empty",true};
	{"NUMBER_L_F3_N", 0, "SpitfireIX_NorwegianAF_AH-G", false};
	{"NUMBER_L_F3_N", DECAL ,"empty",true};
	{"NUMBER_L_F3_NN", 0, "SpitfireIX_NorwegianAF_AH-G", false};
	{"NUMBER_L_F3_NN", DECAL ,"empty",true};

	{"NUMBER_R_0", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_R_0", DECAL ,"empty",true};
	{"NUMBER_R_00", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_R_00", DECAL ,"empty",true};
	{"NUMBER_R_000", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_R_000", DECAL ,"empty",true};

	{"NUMBER_R_F3_0", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_R_F3_0", DECAL ,"empty",true};
	{"NUMBER_R_F3_00", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_R_F3_00", DECAL ,"empty",true};
	{"NUMBER_R_F3_000", 0 ,"SpitfireIX_NorwegianAF_AH-G",false};
	{"NUMBER_R_F3_000", DECAL ,"empty",true};
	{"NUMBER_R_F3_N", 0, "SpitfireIX_NorwegianAF_AH-G", false};
	{"NUMBER_R_F3_N", DECAL ,"empty",true};
	{"NUMBER_R_F3_NN", 0, "SpitfireIX_NorwegianAF_AH-G", false};
	{"NUMBER_R_F3_NN", DECAL ,"empty",true};
}
name = "Blade Norwegian Air Force AH-G"


-- https://www.digitalcombatsimulator.com/en/files/3317624/
-- By Schwert
