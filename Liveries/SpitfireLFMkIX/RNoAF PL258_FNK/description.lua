livery = {
	{"SpitfireIX", 0 ,"SpitfireIX_PL258_DIF",false};
	
	{"NUMBER_L_0", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_0", DECAL ,"empty",true};
	{"NUMBER_L_00", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_00", DECAL ,"empty",true};
	{"NUMBER_L_000", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_000", DECAL ,"empty",true};

	{"NUMBER_L_F3_0", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_F3_0", DECAL ,"empty",true};
	{"NUMBER_L_F3_00", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_F3_00", DECAL ,"empty",true};
	{"NUMBER_L_F3_000", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_F3_000", DECAL ,"empty",true};
	
	{"NUMBER_R_0", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_0", DECAL ,"empty",true};
	{"NUMBER_R_00", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_00", DECAL ,"empty",true};
	{"NUMBER_R_000", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_000", DECAL ,"empty",true};
	
	{"NUMBER_R_F3_0", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_F3_0", DECAL ,"empty",true};
	{"NUMBER_R_F3_00", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_F3_00", DECAL ,"empty",true};
	{"NUMBER_R_F3_000", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_F3_000", DECAL ,"empty",true};
	
	{"NUMBER_R_F3_N", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_F3_N", DECAL ,"empty",true};
	{"NUMBER_R_F3_NN", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_R_F3_NN", DECAL ,"empty",true};
	
	{"NUMBER_L_F3_N", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_F3_N", DECAL ,"empty",true};
	{"NUMBER_L_F3_NN", 0 ,"SpitfireIX_PL258_DIF",false};
	{"NUMBER_L_F3_NN", DECAL ,"empty",true};
	
}
name = "Norwegian Spitfire PL258"


-- https://www.digitalcombatsimulator.com/en/files/3330565/
-- By Sprool
