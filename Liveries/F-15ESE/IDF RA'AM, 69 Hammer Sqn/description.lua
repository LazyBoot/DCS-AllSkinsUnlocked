livery = {
-- NOSE
	{"F-15E_01",	0,	                "F-15I_E01-IDF-69",			false};
	{"F-15E_01",	1,	                "F-15E_E01_NRM",	true};
	{"F-15E_01",	ROUGHNESS_METALLIC,	"F-15I_E01_RoughMet-IDF-69", false};
	{"F-15E_01",	DECAL,	    		"F-15E_DECAL",	true};
	
-- TOP
	{"F-15E_02",	0,	                "F-15I_E02-IDF-69",			false};
	{"F-15E_02",	1,	                "F-15E_E02_NRM",	true};
	{"F-15E_02",	ROUGHNESS_METALLIC,	"F-15I_E02_RoughMet-IDF-69", false};

	{"F-15E_02SB",	0,	                "F-15I_E02-IDF-69",			false};
	{"F-15E_02SB",	1,	                "F-15E_E02_NRM",	true};
	{"F-15E_02SB",	ROUGHNESS_METALLIC,	"F-15I_E02_RoughMet-IDF-69", false};
	
-- BOTTOM
	{"F-15E_03",	0,	                "F-15I_E03-IDF-69",			false};
	{"F-15E_03",	1,	                "F-15E_E03_NRM",	true};
	{"F-15E_03",	ROUGHNESS_METALLIC,	"F-15I_E03_RoughMet-IDF-69", false};
	
-- WING1
	{"F-15E_04",	0,	                "F-15I_E04-IDF-69",			false};
	{"F-15E_04",	1,	                "F-15E_E04_NRM",	true};
	{"F-15E_04",	ROUGHNESS_METALLIC,	"F-15I_E04_RoughMet-IDF-69", false};
 --AILERON1	
 	{"F-15E_04AT",	0,	                "F-15I_E04-IDF-69",			false};
 	{"F-15E_04AT",	1,	                "F-15E_E04_NRM",	true};
 	{"F-15E_04AT",	ROUGHNESS_METALLIC,	"F-15I_E04_RoughMet-IDF-69", false};
 
 	{"F-15E_04AB",	0,	                "F-15I_E04-IDF-69",			false};
 	{"F-15E_04AB",	1,	                "F-15E_E04_NRM",	true};
 	{"F-15E_04AB",	ROUGHNESS_METALLIC,	"F-15I_E04_RoughMet-IDF-69", false};
 --FLAP1
 	{"F-15E_04FT",	0,	                "F-15I_E04-IDF-69",			false};
 	{"F-15E_04FT",	1,	                "F-15E_E04_NRM",	true};
 	{"F-15E_04FT",	ROUGHNESS_METALLIC,	"F-15I_E04_RoughMet-IDF-69", false};
 
 	{"F-15E_04FB",	0,	                "F-15I_E04-IDF-69",			false};
 	{"F-15E_04FB",	1,	                "F-15E_E04_NRM",	true};
 	{"F-15E_04FB",	ROUGHNESS_METALLIC,	"F-15I_E04_RoughMet-IDF-69", false};
 	
--- WING2
 	{"F-15E_05",	0,	                "F-15I_E05-IDF-69",			false};
 	{"F-15E_05",	1,	                "F-15E_E05_NRM",	true};
 	{"F-15E_05",	ROUGHNESS_METALLIC,	"F-15I_E05_RoughMet-IDF-69", false};
 --ALIERON2			
 	{"F-15E_05AT",	0,	                "F-15I_E05-IDF-69",			false};
 	{"F-15E_05AT",	1,	                "F-15E_E05_NRM",	true};
 	{"F-15E_05AT",	ROUGHNESS_METALLIC,	"F-15I_E05_RoughMet-IDF-69", false};
 
 	{"F-15E_05AB",	0,	                "F-15I_E05-IDF-69",			false};
 	{"F-15E_05AB",	1,	                "F-15E_E05_NRM",	true};
 	{"F-15E_05AB",	ROUGHNESS_METALLIC,	"F-15I_E05_RoughMet-IDF-69", false};
 --FLAP2
 	{"F-15E_05FT",	0,	                "F-15I_E05-IDF-69",			false};
 	{"F-15E_05FT",	1,	                "F-15E_E05_NRM",	true};
 	{"F-15E_05FT",	ROUGHNESS_METALLIC,	"F-15I_E05_RoughMet-IDF-69", false};
 
 	{"F-15E_05FB",	0,	                "F-15I_E05-IDF-69",			false};
 	{"F-15E_05FB",	1,	                "F-15E_E05_NRM",	true};
 	{"F-15E_05FB",	ROUGHNESS_METALLIC,	"F-15I_E05_RoughMet-IDF-69", false};
 
--- TAIL
 	{"F-15E_06",	0,	                "F-15I_E06-IDF-69",			false};
 	{"F-15E_06",	1,	                "F-15E_E06_NRM",	true};
 	{"F-15E_06",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", false};
 	{"F-15E_06",	DECAL,	    		"F-15E_DECAL",	true};
 --LEFT STAB
 	{"F-15E_06LS",	0,	                "F-15I_E06-IDF-69",			false};
 	{"F-15E_06LS",	1,	                "F-15E_E06_NRM",	true};
 	{"F-15E_06LS",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", false};
 	{"F-15E_06LS",	DECAL,	    		"F-15E_DECAL",	true};
 --RIGHT STAB	
 	{"F-15E_06RS",	0,	                "F-15I_E06-IDF-69",			false};
 	{"F-15E_06RS",	1,	                "F-15E_E06_NRM",	true};
 	{"F-15E_06RS",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", false};
 	{"F-15E_06RS",	DECAL,	    		"F-15E_DECAL",	true};
 --LEFT RUDDER	
 	{"F-15E_06LR",	0,	                "F-15I_E06-IDF-69",			false};
 	{"F-15E_06LR",	1,	                "F-15E_E06_NRM",	true};
 	{"F-15E_06LR",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", false};
 	{"F-15E_06LR",	DECAL,	    		"F-15E_DECAL",	true};
 --RIGHT RUDDER
 	{"F-15E_06RR",	0,	                "F-15I_E06-IDF-69",			false};
 	{"F-15E_06RR",	1,	                "F-15E_E06_NRM",	true};
 	{"F-15E_06RR",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", false};
 	{"F-15E_06RR",	DECAL,	    		"F-15E_DECAL",	true};

-- CFT
	{"F-15E_07",	0,	                "F-15I_E07-IDF-69",			false};
	{"F-15E_07",	1,	                "F-15E_E07_NRM",	true};
	{"F-15E_07",	ROUGHNESS_METALLIC,	"F-15I_E07_RoughMet-IDF-69",	false};
	{"F-15E_07",	DECAL,	    		"F-15E_DECAL",	true};
	
-- EXTRA
	{"F-15E_08",	0,	                "F-15I_E08-IDF-69",			false};
	{"F-15E_08",	1,	                "F-15E_E08_NRM",	true};
	{"F-15E_08",	ROUGHNESS_METALLIC,	"F-15I_E08_RoughMet-IDF-69", false};
	
-- DROP TANKS
	{"F-15E_13",	0,	                "F-15I_E13-IDF-69",			false};
	{"F-15E_13",	1,	                "F-15E_E13_NRM",	true};
	{"F-15E_13",	ROUGHNESS_METALLIC,	"F-15E_E13_RoughMet", true};
	
-- NAVPOD PYLON
	{"F-15E_14",	0,	                "F-15I_E14-IDF-69",			false};
	{"F-15E_14",	1,	                "F-15E_E14_NRM",	true};
	{"F-15E_14",	ROUGHNESS_METALLIC,	"F-15E_E14_RoughMet", true};
	
-- EXTERIOR COCKPIT
	{"F-15EC_02",	0,	                "F-15I_EC02-IDF-69",			false};
	{"F-15EC_02",	1,	                "F-15E_EC02_NRM",	true};
	{"F-15EC_02",	ROUGHNESS_METALLIC,	"F-15E_EC02_RoughMet", true};
	
	{"F-15EC_03",	0,	                "F-15I_EC03-IDF-69",			false};
	{"F-15EC_03",	1,	                "F-15E_EC03_NRM",	true};
	{"F-15EC_03",	ROUGHNESS_METALLIC,	"F-15E_EC03_RoughMet", true};	
	
	
--COCKPIT---------------------------------------------------------------------------------------

	{"F-15E_INT4",	0,	                "F-15I_T4-BLK",			false};
	{"F-15E_INT4",	1,	                "F-15E_T4_NRM",	true};
	{"F-15E_INT4",	ROUGHNESS_METALLIC,	"F-15e_T4_RoughMet", true};
	
	{"F-15E_INT6",	0,	                "F-15I_T6-BLK",			false};
	{"F-15E_INT6",	1,	                "F-15E_T6_NRM",	true};
	{"F-15E_INT6",	ROUGHNESS_METALLIC,	"F-15e_T6_RoughMet", true};	
	
	{"F-15E_INT7",	0,	                "F-15I_T7-BLK",			false};
	{"F-15E_INT7",	1,	                "F-15E_T7_NRM",	true};
	{"F-15E_INT7",	ROUGHNESS_METALLIC,	"F-15e_T7_RoughMet", true};
	
	{"F-15E_INT8",	0,	                "F-15I_T8-BLK",			false};
	{"F-15E_INT8",	1,	                "F-15E_T8_NRM",	true};
	{"F-15E_INT8",	ROUGHNESS_METALLIC,	"F-15e_T8_RoughMet", true};
	
	{"F-15E_INT11",	0,	                "F-15I_T11-BLK",			false};
	{"F-15E_INT11",	1,	                "F-15E_T11_NRM",	true};
	{"F-15E_INT11",	ROUGHNESS_METALLIC,	"F-15e_T11_RoughMet", true};
	
	{"F-15E_INT12",	0,	                "F-15I_T12-BLK",			false};
	{"F-15E_INT12",	1,	                "F-15E_T12_NRM",	true};
	{"F-15E_INT12",	ROUGHNESS_METALLIC,	"F-15e_T12_RoughMet", true};
	
	{"F-15E_INT14",	0,	                "F-15I_T14-BLK",			false};
	{"F-15E_INT14",	1,	                "F-15E_T14_NRM",	true};
	{"F-15E_INT14",	ROUGHNESS_METALLIC,	"F-15e_T14_RoughMet", true};
	
	{"F-15E_INT26",	0,	                "F-15I_T26-BLK",			false};
	{"F-15E_INT26",	1,	                "F-15E_T26_NRM",	true};
	{"F-15E_INT26",	ROUGHNESS_METALLIC,	"F-15e_T26_RoughMet", true};
	
	{"F-15E_INT27",	0,	                "F-15I_T27-BLK",			false};
	{"F-15E_INT27",	1,	                "F-15E_T27_NRM",	true};
	{"F-15E_INT27",	ROUGHNESS_METALLIC,	"F-15e_T27_RoughMet", true};
	
	{"F-15E_INT28",	0,	                "F-15I_T28-BLK",			false};
	{"F-15E_INT28",	1,	                "F-15E_T28_NRM",	true};
	{"F-15E_INT28",	ROUGHNESS_METALLIC,	"F-15e_T28_RoughMet", true};
	
	{"F-15E_INT29",	0,	                "F-15I_T29-BLK",			false};
	{"F-15E_INT29",	1,	                "F-15E_T29_NRM",	true};
	{"F-15E_INT29",	ROUGHNESS_METALLIC,	"F-15e_T29_RoughMet", true};
	
	{"F-15E_INT30",	0,	                "F-15I_T30-BLK",			false};
	{"F-15E_INT30",	1,	                "F-15E_T30_NRM",	true};
	{"F-15E_INT30",	ROUGHNESS_METALLIC,	"F-15e_T30_RoughMet", true};
	
	{"F-15E_INT31",	0,	                "F-15I_T31-BLK",			false};
	{"F-15E_INT31",	1,	                "F-15E_T31_NRM",	true};
	{"F-15E_INT31",	ROUGHNESS_METALLIC,	"F-15e_T31_RoughMet", true};
	
	{"F-15E_INT32",	0,	                "F-15I_T32-BLK",			false};
	{"F-15E_INT32",	1,	                "F-15E_T32_NRM",	true};
	{"F-15E_INT32",	ROUGHNESS_METALLIC,	"F-15e_T32_RoughMet", true};
	
	{"F-15E_INT35",	0,	                "F-15I_T35-BLK",			false};
	{"F-15E_INT35",	1,	                "F-15E_T35_NRM",	true};
	{"F-15E_INT35",	ROUGHNESS_METALLIC,	"F-15e_T35_RoughMet", true};
	
	{"F-15E_INT37",	0,	                "F-15I_T37-BLK",			false};
	{"F-15E_INT37",	1,	                "F-15E_T37_NRM",	true};
	{"F-15E_INT37",	ROUGHNESS_METALLIC,	"F-15e_T37_RoughMet", true};
	
		
--Ghost Controls	

	{"F-15E_INT4_GHOST",	0,	                "F-15I_T4-BLK_GHOST",			false};
	{"F-15E_INT4",	1,	                "F-15E_T4_NRM",	true};

--NUMBER----------------------------------------------------------------------------------------


--F-15E_NOSE-NUMBER
    {"F-15E_REFUEL-NUMBER",	0,	                "F-15I_NOSE_IDF-69_Numbers",				false};
	
--F-15E_TAIL-NUMBER                             
    {"F-15E_TAIL-NUMBER",	0,	                "F-15I_TAIL_IDF-69_Numbers",				false};

--F-15E_TAIL-NUMBER-L
 	{"F-15E_TAIL-NUMBER-L",	0,	                "F-15I_E06-IDF-69",							false};
 	{"F-15E_TAIL-NUMBER-L",	1,	                "F-15E_E06_NRM",							true};
 	{"F-15E_TAIL-NUMBER-L",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", 				false};
    {"F-15E_TAIL-NUMBER-L",	DECAL,	            "F-15I_TAIL_IDF-69_Numbers",				false};
	
--F-15E_TAIL-NUMBER-R                             
 	{"F-15E_TAIL-NUMBER-R",	0,	                "F-15I_E06-IDF-69",							false};
 	{"F-15E_TAIL-NUMBER-R",	1,	                "F-15E_E06_NRM",							true};
 	{"F-15E_TAIL-NUMBER-R",	ROUGHNESS_METALLIC,	"F-15I_E06_RoughMet-IDF-69", 				false};
    {"F-15E_TAIL-NUMBER-R",	DECAL,	            "F-15I_TAIL_IDF-69_Numbers",				false};


--PILOT
	{"F-15EC_05",	0,	                "F-15E_EC05A1",					true};
	{"F-15EC_05",	1,	                "F-15E_EC05_NRM",				true};
	{"F-15EC_05",	ROUGHNESS_METALLIC,	"F-15E_EC05_RoughMet", 			true};

	{"F-15EC_06",	0,	                "F-15E_EC06A1",					true};
	{"F-15EC_06",	1,	                "F-15E_EC06_NRM",				true};
	{"F-15EC_06",	ROUGHNESS_METALLIC,	"F-15E_EC06_RoughMet", 			true};

	{"F-15EC_07",	0,	                "F-15E_EC07",					true};
	{"F-15EC_07",	1,	                "F-15E_EC07_NRM",				true};
	{"F-15EC_07",	ROUGHNESS_METALLIC,	"F-15E_EC07_RoughMet", 			true};
--    {"F-15EC_07",	DECAL,	            "F-15EC_07_Decal",				true};

--WSO
	{"F-15EC_05R",	0,	                "F-15E_EC05A1",					true};
	{"F-15EC_05R",	1,	                "F-15E_EC05_NRM",				true};
	{"F-15EC_05R",	ROUGHNESS_METALLIC,	"F-15E_EC05_RoughMet", 			true};

	{"F-15EC_06R",	0,	                "F-15E_EC06A1",					true};
	{"F-15EC_06R",	1,	                "F-15E_EC06_NRM",				true};
	{"F-15EC_06R",	ROUGHNESS_METALLIC,	"F-15E_EC06_RoughMet", 			true};

	{"F-15EC_07R",	0,	                "F-15E_EC07",					true};
	{"F-15EC_07R",	1,	                "F-15E_EC07_NRM",				true};
	{"F-15EC_07R",	ROUGHNESS_METALLIC,	"F-15E_EC07_RoughMet", 			true};
--    {"F-15EC_07R",	DECAL,	            "F-15EC_07_Decal",			true};


-----------------------------VR/OTHER PILOTS-----------------------------------------

--PILOT
	{"F-15E_VR-Pilot1",	0,	                "F-15E_TP1A1",					true};
	{"F-15E_VR-Pilot1",	1,	                "F-15E_TP1_NRM",				true};
	{"F-15E_VR-Pilot1",	ROUGHNESS_METALLIC,	"F-15E_TP1_RoughMet", 			true};

	{"F-15E_VR-Pilot2",	0,	                "F-15E_TP2A1",					true};
	{"F-15E_VR-Pilot2",	1,	                "F-15E_TP2_NRM",				true};
	{"F-15E_VR-Pilot2",	ROUGHNESS_METALLIC,	"F-15E_TP2_RoughMet", 			true};

	{"F-15E_VR-Pilot3",	0,	                "F-15E_TP3",					true};
	{"F-15E_VR-Pilot3",	1,	                "F-15E_TP3_NRM",				true};
	{"F-15E_VR-Pilot3",	ROUGHNESS_METALLIC,	"F-15E_TP3_RoughMet", 			true};

--WSO
	{"F-15E_VR-Pilot1R",	0,	                "F-15E_TP1A1",				true};
	{"F-15E_VR-Pilot1R",	1,	                "F-15E_TP1_NRM",			true};
	{"F-15E_VR-Pilot1R",	ROUGHNESS_METALLIC,	"F-15E_TP1_RoughMet", 		true};

	{"F-15E_VR-Pilot2R",	0,	                "F-15E_TP2A1",				true};
	{"F-15E_VR-Pilot2R",	1,	                "F-15E_TP2_NRM",			true};
	{"F-15E_VR-Pilot2R",	ROUGHNESS_METALLIC,	"F-15E_TP2_RoughMet", 		true};

	{"F-15E_VR-Pilot3R",	0,	                "F-15E_TP3",				true};
	{"F-15E_VR-Pilot3R",	1,	                "F-15E_TP3_NRM",			true};
	{"F-15E_VR-Pilot3R",	ROUGHNESS_METALLIC,	"F-15E_TP3_RoughMet", 		true};

--Visor
	{"F-15EC_07_Visor",	0,	                "F-15E_EC07",					true};
	{"F-15EC_07_Visor",	1,	                "F-15E_EC07_NRM",				true};
	{"F-15EC_07_Visor",	ROUGHNESS_METALLIC,	"F-15E_EC07_RoughMet", 		true};

	{"F-15EC_07R_Visor",	0,	                "F-15E_EC07",					true};
	{"F-15EC_07R_Visor",	1,	                "F-15E_EC07_NRM",				true};
	{"F-15EC_07R_Visor",	ROUGHNESS_METALLIC,	"F-15E_EC07_RoughMet", 			true};
}



name = "IDF 69th Hammers Scheme B" 
 -- Israel

custom_args = 
{
-- [32]   = 0.7, -- 1
-- [31]   = 0.6, -- 10
-- [442]  = 0.2, -- 100 
[1000] = 0.4, -- change of type of board number (0.0 - No Numbers, 0.1 - 3 Numbers, 0.2 - 4 Numbers starts with "1", 0.3 - Full 4 Numbers, 0.4 - IDF Numbers)
[1001] = 0.0, -- vis refuel board number (0.0 - No Numbers, 0.1 Numbers, 0.2 - Numbers Early)
[1002] = 0.4, -- change of type intake board number (0.4 - IDF Nose/Fuselage Numbers and Model Change)
[1003] = 0.4, -- vis Nose Gear board number (0.0 - 4 Numbers 0.0 to 0.05 adjusts the placement, 0.1 - 5 Numbers, 0.4 - No Numbers)

}


