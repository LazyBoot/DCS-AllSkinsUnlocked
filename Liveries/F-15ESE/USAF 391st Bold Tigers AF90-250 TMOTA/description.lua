livery = {
-- NOSE
	{"F-15E_01",	0,	                "F-15e_E01_TIGER",		true};
	{"F-15E_01",	1,	                "F-15e_E01_NRM",	true};
	{"F-15E_01",	ROUGHNESS_METALLIC,	"F-15e_E01_RoughMet_TIGER", true};
	{"F-15E_01",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
	
-- TOP
	{"F-15E_02",	0,	                "F-15e_E02_TIGER",			true};
	{"F-15E_02",	1,	                "F-15e_E02_NRM",	true};
	{"F-15E_02",	ROUGHNESS_METALLIC,	"F-15e_E02_RoughMet_TIGER", true};
	
	{"F-15E_02SB",	0,	                "F-15e_E02_TIGER",			true};
	{"F-15E_02SB",	1,	                "F-15e_E02_NRM",	true};
	{"F-15E_02SB",	ROUGHNESS_METALLIC,	"F-15e_E02_RoughMet_TIGER", true};	
	
-- TAIL
	{"F-15E_06",	0,	                "F-15e_E06_TIGER",		true};
	{"F-15E_06",	1,	                "F-15e_E06_NRM",	true};
	{"F-15E_06",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", true};
	{"F-15E_06",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
--LEFT STAB
	{"F-15E_06LS",	0,	                "F-15e_E06_TIGER",	true};
	{"F-15E_06LS",	1,	                "F-15e_E06_NRM",	true};
	{"F-15E_06LS",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", true};
	{"F-15E_06LS",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
--RIGHT STAB	
	{"F-15E_06RS",	0,	                "F-15e_E06_TIGER",	true};
	{"F-15E_06RS",	1,	                "F-15e_E06_NRM",	true};
	{"F-15E_06RS",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", true};
	{"F-15E_06RS",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
--LEFT RUDDER	
	{"F-15E_06LR",	0,	                "F-15e_E06_TIGER",	true};
	{"F-15E_06LR",	1,	                "F-15e_E06_NRM",	true};
	{"F-15E_06LR",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", true};
	{"F-15E_06LR",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
--RIGHT RUDDER
	{"F-15E_06RR",	0,	                "F-15e_E06_TIGER",	true};
	{"F-15E_06RR",	1,	                "F-15e_E06_NRM",	true};
	{"F-15E_06RR",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", true};
	{"F-15E_06RR",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
	
-- CFT
	{"F-15E_07",	0,	                "F-15e_E07_TIGER",		true};
	{"F-15E_07",	1,	                "F-15e_E07_NRM",	true};
	{"F-15E_07",	ROUGHNESS_METALLIC,	"F-15e_E07_RoughMet_TIGER",	true};
	{"F-15E_07",	DECAL,	    		"F-15E_DECAL-TIGER",	true};
	
-- EXTERIOR COCKPIT
	{"F-15EC_02",	0,	                "F-15E_EC02_TIGER",			true};
	{"F-15EC_02",	1,	                "F-15E_EC02_NRM",	true};
	{"F-15EC_02",	ROUGHNESS_METALLIC,	"F-15E_EC02_RoughMet", true};
	
	

--BORT_NUMBER----------------------------------------------------------------------------------------



--F-15E_REFUEL-DECAL	
    {"F-15E_REFUEL-DECAL",	0,	                "F-15E_REFUEL_AF-90-0",				true};

--F-15E_REFUEL-NUMBER
    {"F-15E_REFUEL-NUMBER",	0,	                "F-15E_WHT_Numbers",				true};

--F-15E_TAIL-DECAL	
    {"F-15E_TAIL-DECAL",	0,	                "F-15E_TAIL_AF-90-0",				true};
	
--F-15E_TAIL-DECAL-L	
    {"F-15E_TAIL-DECAL-L",	0,	                "F-15E_E06_TIGER",					true};
	{"F-15E_TAIL-DECAL-L",	1,	                "F-15e_E06_NRM",				true};
	{"F-15E_TAIL-DECAL-L",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", 			true};
    {"F-15E_TAIL-DECAL-L",	DECAL,	            "F-15E_TAIL_AF-90-L-ORG-Shadow",				true};
	
--F-15E_TAIL-DECAL-R
    {"F-15E_TAIL-DECAL-R",	0,	                "F-15E_E06_TIGER",					true};
	{"F-15E_TAIL-DECAL-R",	1,	                "F-15e_E06_NRM",				true};
	{"F-15E_TAIL-DECAL-R",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", 			true};
    {"F-15E_TAIL-DECAL-R",	DECAL,	            "F-15E_TAIL_AF-90-R-ORG-Shadow",				true};
	
--F-15E_TAIL-NUMBER                             
    {"F-15E_TAIL-NUMBER",	0,	                "F-15E_BLK_Numbers",			true};
	
--F-15E_TAIL-NUMBER-L
	{"F-15E_TAIL-NUMBER-L",	0,	                "F-15e_E06_TIGER",					true};
	{"F-15E_TAIL-NUMBER-L",	1,	                "F-15e_E06_NRM",				true};
	{"F-15E_TAIL-NUMBER-L",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", 			true};
    {"F-15E_TAIL-NUMBER-L",	DECAL,	            "F-15E_L_ORG_Numbers",			true};

--F-15E_TAIL-NUMBER-R                            
	{"F-15E_TAIL-NUMBER-R",	0,	                "F-15e_E06_TIGER",					true};
	{"F-15E_TAIL-NUMBER-R",	1,	                "F-15e_E06_NRM",				true};
	{"F-15E_TAIL-NUMBER-R",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet_TIGER", 			true};
    {"F-15E_TAIL-NUMBER-R",	DECAL,	            "F-15E_R_ORG_Numbers",			true};


--PILOT
	{"F-15EC_05",	0,	                "F-15E_EC05A1",					true};
	{"F-15EC_05",	1,	                "F-15E_EC05_NRM",				true};
	{"F-15EC_05",	ROUGHNESS_METALLIC,	"F-15E_EC05_RoughMet", 			true};

	{"F-15EC_06",	0,	                "F-15E_EC06A1",					true};
	{"F-15EC_06",	1,	                "F-15E_EC06_NRM",				true};
	{"F-15EC_06",	ROUGHNESS_METALLIC,	"F-15E_EC06_RoughMet", 			true};

	{"F-15EC_07",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 		true};
--    {"F-15EC_07",	DECAL,	            "F-15EC_07_Decal",			true};

--WSO
	{"F-15EC_05R",	0,	                "F-15E_EC05A1",					true};
	{"F-15EC_05R",	1,	                "F-15E_EC05_NRM",				true};
	{"F-15EC_05R",	ROUGHNESS_METALLIC,	"F-15E_EC05_RoughMet", 			true};

	{"F-15EC_06R",	0,	                "F-15E_EC06A1",					true};
	{"F-15EC_06R",	1,	                "F-15E_EC06_NRM",				true};
	{"F-15EC_06R",	ROUGHNESS_METALLIC,	"F-15E_EC06_RoughMet", 			true};

	{"F-15EC_07R",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07R",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07R",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 			true};
--    {"F-15EC_07R",	DECAL,	            "F-15EC_07_Decal",			true};


-----------------------------VR/OTHER PILOTS-----------------------------------------

--PILOT
	{"F-15E_VR-Pilot1",	0,	                "F-15E_TP1A1",					true};
	{"F-15E_VR-Pilot1",	1,	                "F-15E_TP1_NRM",				true};
	{"F-15E_VR-Pilot1",	ROUGHNESS_METALLIC,	"F-15E_TP1_RoughMet", 			true};

	{"F-15E_VR-Pilot2",	0,	                "F-15E_TP2A1",					true};
	{"F-15E_VR-Pilot2",	1,	                "F-15E_TP2_NRM",				true};
	{"F-15E_VR-Pilot2",	ROUGHNESS_METALLIC,	"F-15E_TP2_RoughMet", 			true};

	{"F-15E_VR-Pilot3",	0,	                "F-15E_TP3",					true};
	{"F-15E_VR-Pilot3",	1,	                "F-15E_TP3_NRM",				true};
	{"F-15E_VR-Pilot3",	ROUGHNESS_METALLIC,	"F-15E_TP3_RoughMet", 			true};

--WSO
	{"F-15E_VR-Pilot1R",	0,	                "F-15E_TP1A1",				true};
	{"F-15E_VR-Pilot1R",	1,	                "F-15E_TP1_NRM",			true};
	{"F-15E_VR-Pilot1R",	ROUGHNESS_METALLIC,	"F-15E_TP1_RoughMet", 		true};

	{"F-15E_VR-Pilot2R",	0,	                "F-15E_TP2A1",				true};
	{"F-15E_VR-Pilot2R",	1,	                "F-15E_TP2_NRM",			true};
	{"F-15E_VR-Pilot2R",	ROUGHNESS_METALLIC,	"F-15E_TP2_RoughMet", 		true};

	{"F-15E_VR-Pilot3R",	0,	                "F-15E_TP3",				true};
	{"F-15E_VR-Pilot3R",	1,	                "F-15E_TP3_NRM",			true};
	{"F-15E_VR-Pilot3R",	ROUGHNESS_METALLIC,	"F-15E_TP3_RoughMet", 		true};

--Visor
	{"F-15EC_07_Visor",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07_Visor",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07_Visor",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 		true};

	{"F-15EC_07R_Visor",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07R_Visor",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07R_Visor",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 			true};
}



name = "USAF 391ST Bold Tigers 2005 Tiger Meet of the Americas" --0250


custom_args = 
{
[32]   = 0.0, -- 1
[31]   = 0.5, -- 10
[442]  = 0.2, -- 100 
[1000] = 0.1, -- change of type of board number (0.0 - No Numbers, 0.1 - 3 Numbers, 0.2 - 4 Numbers starts with "1", 0.3 - Full 4 Numbers)
[1001] = 0.1, -- vis refuel board number (0.0 - No Numbers, 0.1 Numbers, 0.2 - Numbers Early)
[1002] = 0.0, -- change of type intake board number 
[1003] = 0.02, -- vis Nose Gear board number (0.0 - 4 Numbers 0.0 to 0.05 adjusts the placement, 0.1 - 5 Numbers) 
}


