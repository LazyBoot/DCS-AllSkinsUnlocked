livery = {

	{"f18c1", 0 ,"F18C_1_DIFF_323_high visibility",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	
	{"f18c2", 0 ,"F18C_2_DIFF_323_high visibility",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};

	
			
	
	
	{"f18c1_number_nose_right", 0 ,"F18C_1_DIFF_323_high visibility",false};
	{"f18c1_number_nose_right", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_right", DECAL ,"vmfa-323_right",false};
	
	{"f18c1_number_nose_left", 0 ,"F18C_1_DIFF_323_high visibility",false};
	{"f18c1_number_nose_left", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_left", DECAL ,"vmfa-323_left",false};	

	{"f18c2_kil_right", 0 ,"F18C_2_DIFF_323_high visibility",false};
	{"f18c2_kil_right", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_right", DECAL ,"vmfa-323_wings",false};
	

	{"f18c2_kil_left", 0 ,"F18C_2_DIFF_323_high visibility",false};
	{"f18c2_kil_left", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_left", DECAL ,"vmfa-323_wings",false};
	
	{"f18c1_number_F", 0 ,"F18C_1_DIFF_323_high visibility",false};
	{"f18c1_number_F", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_F", DECAL ,"empty",true};	

	{"f18c2_number_X", 0 ,"F18C_2_DIFF_323_high visibility",false};
	{"f18c2_number_X", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_number_X", DECAL ,"vmfa-323_wings",false};	
	
	
	


	{"FPU_8A", 0 ,"FPU_8A_323_high visibility",false};
	{"FPU_8A", 2 ,"FPU_8A_Diff_RoughMet",true};
	


	



}
name = "VMFA-323_high visibility"






custom_args = 
{
 
[27] = 0.0, -- Tail      change of type of board number (0.0 -default USA, 0.1- )
[1000] = 0.0, -- Flaps
[1001] = 0.0, -- Nose
[1002] = 1, -- Kuwait Squadron 
[1003] = 1, -- Australian Squadron 
[1004] = 1, -- Finland Squadron
[1005] = 1, -- Switzerland Squadron
[1006] = 1, -- Blue Angels Jet Team


}









	

