livery = {

	{"f18c1", 0 ,"F18C_1_DIFF_Maverick",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	
	{"f18c2", 0 ,"F18C_1_DIFF_2_Maverick",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};

	
					
	
	{"f18c1_number_nose_right", 0 ,"F18C_1_DIFF_Maverick",false};
	{"f18c1_number_nose_right", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_right", DECAL ,"empty",true};
	
	{"f18c1_number_nose_left", 0 ,"F18C_1_DIFF_Maverick",false};
	{"f18c1_number_nose_left", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_left", DECAL ,"empty",true};	

	{"f18c2_kil_right", 0 ,"F18C_1_DIFF_2_Maverick",false};
	{"f18c2_kil_right", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_right", DECAL ,"empty",true};
	

	{"f18c2_kil_left", 0 ,"F18C_1_DIFF_2_Maverick",false};
	{"f18c2_kil_left", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_left", DECAL ,"empty",true};
	
	{"f18c1_number_F", 0 ,"F18C_1_DIFF_Maverick",false};
	{"f18c1_number_F", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_F", DECAL ,"empty",true};	

	{"f18c2_number_X", 0 ,"F18C_1_DIFF_2_Maverick",false};
	{"f18c2_number_X", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_number_X", DECAL ,"F18C_bort_number2",true};	
	
	
	
	{"pilot_F18_patch", 0 ,"pilot_F18_patch_maverick",false};
	{"pilot_F18_patch", 1 ,"pilot_F18_patch_maverick_nm",false};
	{"pilot_F18_patch", ROUGHNESS_METALLIC ,"pilot_F18_patch_maverick_roughmet",false};
    
        {"pilot_F18_helmet", 0 ,"pilot_F18_helmet_maverick",false};
	{"pilot_F18_helmet", 2 ,"pilot_F18_helmet_roughmet",true};
	
	


	{"FPU_8A", 0 ,"FPU_8A_Maverick",false};
	{"FPU_8A", 2 ,"FPU_8A_Diff_RoughMet",true};




	
}
name = "Maverick"

order     = 1000 -- to keep in end of the list




custom_args = 
{
 
[27] = 1, -- Tail      change of type of board number (0.0 -default USA, 0.1- )
[1000] = 0.0, -- Flaps
[1001] = 1, -- Nose
[1002] = 1, -- Kuwait Squadron 
[1003] = 1, -- Australian Squadron 
[1004] = 1, -- Finland Squadron
[1005] = 1, -- Switzerland Squadron
[1006] = 1, -- Blue Angels Jet Team


}

	



	

	




	

