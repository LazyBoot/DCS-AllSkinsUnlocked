livery = {

	{"f18c1", 0 ,"F18C_1_DIF_Kuwait25",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	
	{"f18c2", 0 ,"F18C_2_DIF_Kuwait25",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};

	
	
	
	{"f18c1_number_nose_right", 0 ,"F18C_1_DIF_Kuwait25",false};
	{"f18c1_number_nose_right", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_right", DECAL ,"empty",true};
	
	{"f18c1_number_nose_left", 0 ,"F18C_1_DIF_Kuwait25",false};
	{"f18c1_number_nose_left", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_left", DECAL ,"empty",true};	

	{"f18c2_kil_right", 0 ,"F18C_2_DIF_Kuwait25",false};
	{"f18c2_kil_right", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_right", DECAL ,"empty",true};
	

	{"f18c2_kil_left", 0 ,"F18C_2_DIF_Kuwait25",false};
	{"f18c2_kil_left", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_left", DECAL ,"empty",true};
	
	{"f18c1_number_F", 0 ,"F18C_1_DIF_Kuwait25",false};
	{"f18c1_number_F", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_F", DECAL ,"F18C_bort_number_Kuwait",false};	

	{"f18c2_number_X", 0 ,"F18C_2_DIF_Kuwait25",false};
	{"f18c2_number_X", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_number_X", DECAL ,"F18C_bort_number_Kuwait",false};

	{"pilot_F18", 0 ,"pilot_F18",true};
	{"pilot_F18_helmet", 0 ,"pilot_F18_helmet",true};
	{"pilot_F18_patch", 0 ,"empty",true};


	
}
name = "9th Squadron"



custom_args = 
{
 
[27] = 1, -- Tail      change of type of board number (0.0 -default USA, 0.1- )
[1000] = 1, -- Flaps
[1001] = 1, -- Nose
[1002] = 0.0, -- Kuwait Squadron 
[1003] = 1, -- Australian Squadron 
[1004] = 1, -- Finland Squadron
[1005] = 1, -- Switzerland Squadron
[1006] = 1, -- Blue Angels Jet Team


}
	
	

