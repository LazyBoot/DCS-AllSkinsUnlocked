livery = {

	{"f18c1", 0 ,"F18C_1_DIFF_1_Spain_111_73",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	
	{"f18c2", 0 ,"F18C_1_DIFF_2_Spain_111_73",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};

	
	
	




	{"pilot_F18", 0 ,"pilot_F18",true};
	{"pilot_F18_helmet", 0 ,"pilot_F18_helmet",true};
	{"pilot_F18_patch", 0 ,"pilot_F18_patch_111_73",false};
	{"pilot_F18_patch", 1 ,"pilot_F18_patch_11_nm",false};
	{"pilot_F18_patch", ROUGHNESS_METALLIC ,"pilot_F18_patch_11_RoughMet",false};	
	

	
}
name = "Spain 111 Escuadron C.15-73"


custom_args = 
{
 
[27] = 1, -- Tail      change of type of board number (0.0 -default USA, 0.1- )
[1000] = 1, -- Flaps
[1001] = 1, -- Nose
[1002] = 1, -- Kuwait Squadron 
[1003] = 1, -- Australian Squadron 
[1004] = 1, -- Finland Squadron
[1005] = 1, -- Switzerland Squadron
[1006] = 1, -- Blue Angels Jet Team


}
