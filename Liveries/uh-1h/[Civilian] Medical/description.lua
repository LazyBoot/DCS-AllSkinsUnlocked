livery = {
	{"civ_uh1_main_dam", 0, "civ_uh1_med", true};	
	{"civ_uh1_tail_dam", 0, "civ_uh1_tail_med", true};	
	{"civ_uh1_main", 0, "civ_uh1_med", true};	
	{"civ_uh1_tail", 0, "civ_uh1_tail_med", true};	

	{"uh1_main_dam", 0, "civ_uh1_med", true};	
	{"uh1_tail_dam", 0, "civ_uh1_tail_med", true};	
	{"uh1_main", 0, "civ_uh1_med", true};	
	{"uh1_tail", 0, "civ_uh1_tail_med", true};			
	{"uh1_weapon", 0, "uh1_weapon", true};
	{"pilot_UH1_01", 0, "pilot_UH1_01", true};
	{"pilot_UH1_02", 0, "pilot_UH1_02", true};
	{"pilot_UH1_helmet", 0, "pilot_UH1_helmet", true};
	{"pilot_UH1_patch", 0, "pilot_UH1_patch_1_cavalery", true};	
	{"uh1_cov", 0, "uh1_cover", true};
	{"UH1-cpt-door", 0 ,"UH1_door_c",true};
	{"UH1-cpt_karkas", 0 ,"UH1_karkas_c",true};
	{"UH1-cpt_karkas2", 0 ,"UH1_karkas2_c",true};
	{"UH1-cpt_panel", 0 ,"UH1_panel_c",true};
	{"UH1-cpt_central_box", 0 ,"UH1_boxs_c",true};
	{"UH1-cpt_dev2", 0 ,"UH1_dev_2",true};
	
	{"uh1_main_dam", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"uh1_tail_dam", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	{"uh1_main", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"uh1_tail", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	
	{"civ_uh1_main_dam", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"civ_uh1_tail_dam", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	{"civ_uh1_main", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"civ_uh1_tail", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	
}


