livery = {
	{"civ_uh1_main_dam", 0, "uh1_marina_italiana", false};
	{"civ_uh1_tail_dam", 0, "uh1_tail_marina_italiana", false};
	{"civ_uh1_main", 0, "uh1_marina_italiana", false};
	{"civ_uh1_tail", 0, "uh1_tail_marina_italiana", false};	
	
	{"uh1_main_dam", 0, "uh1_marina_italiana", false};
	{"uh1_tail_dam", 0, "uh1_tail_marina_italiana", false};
	{"uh1_main", 0, "uh1_marina_italiana", false};
	{"uh1_tail", 0, "uh1_tail_marina_italiana", false};
	{"uh1_weapon", 0, "uh1_weapon_blk", true};
	{"pilot_UH1_01", 0, "pilot_uh1_marina_italiana", false};
	{"pilot_UH1_02", 0, "pilot_UH1_02", true};
	{"pilot_UH1_helmet", 0, "pilot_UH1_helmet_marina_italiana", false};
	{"pilot_UH1_patch", 0, "pilot_UH1_patch_UN", false};
	{"uh1_cov", 0, "uh1_marina_italiana_cover", false};
	{"UH1-cpt-door", 0 ,"UH1_door_c",true};
	{"UH1-cpt_karkas", 0 ,"UH1_karkas_c",true};
	{"UH1-cpt_karkas2", 0 ,"UH1_karkas2_c",true};
	{"UH1-cpt_panel", 0 ,"UH1_panel_c",true};
	{"UH1-cpt_central_box", 0 ,"UH1_boxs_c",true};
	{"UH1-cpt_dev2", 0 ,"UH1_dev_2",true};
	
	{"uh1_main_dam", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"uh1_tail_dam", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	{"uh1_main", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"uh1_tail", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	
	{"civ_uh1_main_dam", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"civ_uh1_tail_dam", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
	{"civ_uh1_main", ROUGHNESS_METALLIC,"uh1_RoughMet",true};	
	{"civ_uh1_tail", ROUGHNESS_METALLIC,"uh1_tail_RoughMet",true};	
}
name = "Marina Militare s.n. 80951 7-20"

