livery = {
  {"pilot_SU27", 0, "pilot_su27", false};
  {"pilot_SU27_helmet", 0, "pilot_su27_helmet", false};
  {"pilot_SU27_patch", 0, "pilot_su27_patch", false};
  {"pilot_SU_body", 0, "pilot_cnhh", false};  
  {"su27_tex01", 0 ,"Su27_tex01",false};
  {"su27_tex01", 3 ,"empty",true};
  {"su27_tex01_BN31", 0 ,"Su27_tex01",false};
  {"su27_tex01_BN31", 3 ,"su-27_numbers_PLA_BD",false};
  {"su27_tex01_BN32", 0 ,"Su27_tex01",false};
  {"su27_tex01_BN32", 3 ,"su-27_numbers_PLA_BD",false};
  {"su27_tex02", 0 ,"Su27_tex02",false};
  {"su27_tex02_compos", 0 ,"Su27_tex02",false};
  {"su27_tex03", 0 ,"Su27_tex03",false};
  {"su27_tex03_compos", 0 ,"Su27_tex03",false};
  {"su27_tex03_compos", 3 ,"empty",true};
  {"su27_tex04", 0 ,"Su27_tex04",false};
  {"su27_tex04", 3 ,"empty",true};
  {"su27_tex05", 0 ,"Su27_tex05",false};
  {"su27_tex05", 3 ,"empty",true};
  {"su27_tex06", 0 ,"Su27_tex06",false};
  {"su27_tex06", 3 ,"empty",true};
  {"su27_tex07", 0 ,"Su27_tex07",false};
  
  -- spec
  --{"su27_tex01", 2 ,"Su27_tex01_spc",false};
  --{"su27_tex01_BN31", 2 ,"Su27_tex01_spc",false};
  --{"su27_tex01_BN32", 2 ,"Su27_tex01_spc",false};
  --{"su27_tex02", 2 ,"Su27_tex02_spc",false};
  --{"su27_tex02_compos", 2 ,"Su27_tex02_spc",false};
  --{"su27_tex03", 2 ,"Su27_tex03_spc",false};
  --{"su27_tex03_compos", 2 ,"Su27_tex03_spc",false};
  --{"su27_tex04", 2 ,"Su27_tex04_spc",false};
  --{"su27_tex05", 2 ,"Su27_tex05_spc",false};
  --{"su27_tex06", 2 ,"Su27_tex06_spc",false};
  --{"su27_tex07", 2 ,"Su27_tex07_spc",false};  
  
  {"su27_nose_numbers", 0 ,"empty",true};
  {"su27_tail_numbers_down", 0 ,"empty",true};
  {"su27_tail_numbers", 0, "empty", true};
  {"su27_tail_numbers_top", 0, "su-27_numbers_PLA_BD", false};
  
  {"su27_tex01", ROUGHNESS_METALLIC ,"Su27_tex01_RoughMet",true};	
	{"su27_tex01_BN31", ROUGHNESS_METALLIC ,"Su27_tex01_RoughMet",true};
	{"su27_tex01_BN32", ROUGHNESS_METALLIC ,"Su27_tex01_RoughMet",true};	
	{"su27_tex02", ROUGHNESS_METALLIC ,"Su27_tex02_RoughMet",true};
	{"su27_tex02_compos", ROUGHNESS_METALLIC ,"Su27_tex02_RoughMet",true};
	{"su27_tex03", ROUGHNESS_METALLIC ,"Su27_tex03_RoughMet",true};
	{"su27_tex03_compos", ROUGHNESS_METALLIC ,"Su27_tex03_RoughMet",true};
	{"su27_tex04", ROUGHNESS_METALLIC ,"Su27_tex04_RoughMet",true};
	{"su27_tex05", ROUGHNESS_METALLIC ,"Su27_tex05_RoughMet",true};	
	{"su27_tex06", ROUGHNESS_METALLIC ,"Su27_tex06_RoughMet",true};
	{"su27_tex07", ROUGHNESS_METALLIC ,"Su27_tex07_RoughMet",true};	
	{"su27_detail", ROUGHNESS_METALLIC ,"Su-27_detail_RoughMet",true};	
}
name = "PLANAF HH8S"
name_cn = "海航8师"

-- livery by uboats

