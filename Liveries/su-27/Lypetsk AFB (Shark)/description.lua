livery = {
	{"su27_tex01", 0 ,"Su27_tex01_shark",false};
	{"su27_tex01", 3 ,"empty",true};
	{"su27_tex01_BN31", 0 ,"Su27_tex01_shark",false};
	{"su27_tex01_BN31", 3 ,"Su-27_numbers_red",false};
	{"su27_tex01_BN32", 0 ,"Su27_tex01_shark",false};
	{"su27_tex01_BN32", 3 ,"Su-27_numbers_red",false};
	{"su27_tex02", 0 ,"Su27_tex02_shark",false};
	{"su27_tex02_compos", 0 ,"Su27_tex02_shark",false};
	{"su27_tex03", 0 ,"Su27_tex03_shark",false};
	{"su27_tex03_compos", 0 ,"Su27_tex03_shark",false};
	{"su27_tex03_compos", 3 ,"empty",true};
	{"su27_tex04", 0 ,"Su27_tex04_shark",false};
	{"su27_tex04", 3 ,"empty",true};
	{"su27_tex05", 0 ,"Su27_tex05_shark",false};
	{"su27_tex05", 3 ,"empty",true};
	{"su27_tex06", 0 ,"Su27_tex06_shark",false};
	{"su27_tex06", 3 ,"empty",true};
	{"su27_tex07", 0 ,"Su27_tex07_shark",false};
	{"su27_nose_numbers", 0 ,"Su-27_numbers_red",false};
	{"su27_tail_numbers", 0 ,"Su-27_numbers_red",false};
	{"su27_tail_numbers_top", 0 ,"empty",true};
	{"su27_tail_numbers_down", 0 ,"empty",true};
	{"su27_detail", 0 ,"Su-27_detail_shark",false};
	{"su27_detail_compos", 0 ,"Su-27_detail_shark",false};	
	
	{"su27_tex01", ROUGHNESS_METALLIC ,"Su27_tex01_RoughMet",true};	
	{"su27_tex01_BN31", ROUGHNESS_METALLIC ,"Su27_tex01_RoughMet",true};
	{"su27_tex01_BN32", ROUGHNESS_METALLIC ,"Su27_tex01_RoughMet",true};	
	{"su27_tex02", ROUGHNESS_METALLIC ,"Su27_tex02_RoughMet",true};
	{"su27_tex02_compos", ROUGHNESS_METALLIC ,"Su27_tex02_RoughMet",true};
	{"su27_tex03", ROUGHNESS_METALLIC ,"Su27_tex03_shark_RoughMet",false};
	{"su27_tex03_compos", ROUGHNESS_METALLIC ,"Su27_tex03_shark_RoughMet",false};
	{"su27_tex04", ROUGHNESS_METALLIC ,"Su27_tex04_shark_RoughMet",false};
	{"su27_tex05", ROUGHNESS_METALLIC ,"Su27_tex05_RoughMet",true};	
	{"su27_tex06", ROUGHNESS_METALLIC ,"Su27_tex06_RoughMet",true};
	{"su27_tex07", ROUGHNESS_METALLIC ,"Su27_tex07_RoughMet",true};	
	{"su27_detail", ROUGHNESS_METALLIC ,"Su-27_detail_shark_RoughMet",false};
	{"su27_detail_compos", ROUGHNESS_METALLIC ,"Su-27_detail_shark_RoughMet",false};
}
name_ru = "'Акула' (Липецкий авиацентр)"


